package com.appetiser.module.common

fun Double.toCurrencyFormat() = String.format("$ %.2f", this)
