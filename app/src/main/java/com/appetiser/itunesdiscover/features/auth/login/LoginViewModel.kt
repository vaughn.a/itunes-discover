package com.appetiser.itunesdiscover.features.auth.login

import android.os.Bundle
import com.appetiser.itunesdiscover.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.mapper.getUserSession
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    private lateinit var email: String

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        email = bundle?.getString(LoginActivity.KEY_EMAIL, "")!!
        disposables.add(Observable.just(email)
            .observeOn(schedulers.ui())
            .subscribeBy(onNext = { _state.onNext(LoginState.GetEmail(it)) }))
    }

    private val _state by lazy {
        PublishSubject.create<LoginState>()
    }

    val state: Observable<LoginState> = _state

    fun login(email: String, password: String) {
        repository
            .login(email, password)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(LoginState.ShowProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(LoginState.HideProgressLoading)

                    val user = it.getUserSession()
                    if (user.id.orEmpty().isNotEmpty()) {
                        if (user.verified) {
                            if (user.hasOnboardingDetails()) {
                                _state.onNext(LoginState.LoginSuccess(user))
                            } else {
                                _state.onNext(LoginState.NoUserFirstAndLastName)
                            }
                        } else {
                            _state.onNext(LoginState.UserNotVerified(user))
                        }
                    }
                }, onError = {
                _state.onNext(LoginState.Error(it))
            })

            .apply { disposables.add(this) }
    }
}
