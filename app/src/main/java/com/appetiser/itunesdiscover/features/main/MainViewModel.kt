package com.appetiser.itunesdiscover.features.main

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.itunesdiscover.base.BaseViewModel
import com.appetiser.itunesdiscover.features.main.models.MediaItem
import com.appetiser.module.data.features.main.MediaRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val repo: MediaRepository
) : BaseViewModel() {

    companion object {
        const val emptyStringAscii = "%22%22"
    }

    private val _state by lazy {
        PublishSubject.create<MainState>()
    }
    val state: Observable<MainState> = _state

    private val _selectedTab = MutableLiveData<Int>()
    val selectedTab: LiveData<Int> = _selectedTab

    var selectedTabStr = MutableLiveData<String>()

    private val _feed = MutableLiveData<List<MediaItem>>()
    val feed: LiveData<List<MediaItem>> = _feed

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        filterFeedData("star", "AU", "all")
    }

    internal fun filterFeedData(
        term: String,
        country: String,
        media: String
    ) {

        val termElseEmptyAscii = if (term.trim() == "") emptyStringAscii else term

        repo.getMediaFromRemote(
            termElseEmptyAscii,
            country,
            media
        )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(MainState.ShowProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    // Do insert to local
                    repo.saveMediaFeed(it)

                    // Todo: or... save to vm..
                    it.map {
                        MediaItem(it)
                    }.apply {
                        _feed.postValue(this)
                    }
                },
                onError = {
                    Timber.e(it.localizedMessage)
                }
            )
            .apply {
                _state.onNext(MainState.HideProgressLoading)
                disposables.add(this)
            }
    }

    internal fun changeMainState(pos: Int, typeString: String) {
        _selectedTab.postValue(pos)
        selectedTabStr.postValue(typeString)
        _state.onNext(MainState.MediaTypeByIndex(typeString))
    }
}
