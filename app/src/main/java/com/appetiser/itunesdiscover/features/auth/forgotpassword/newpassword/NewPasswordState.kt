package com.appetiser.itunesdiscover.features.auth.forgotpassword.newpassword

sealed class NewPasswordState {

    data class Success(val email: String) : NewPasswordState()

    data class Error(val throwable: Throwable) : NewPasswordState()

    object ShowProgressLoading : NewPasswordState()

    object HideProgressLoading : NewPasswordState()
}
