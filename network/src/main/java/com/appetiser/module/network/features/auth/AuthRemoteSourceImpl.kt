package com.appetiser.module.network.features.auth

import com.appetiser.module.domain.features.auth.models.AccessToken
import com.appetiser.module.domain.features.auth.models.CountryCode
import com.appetiser.module.domain.features.auth.models.UserSession
import com.appetiser.module.network.Stubs
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.auth.models.UserSessionDTO
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthRemoteSourceImpl @Inject constructor(
    private val baseplateApiServices: BaseplateApiServices,
    private val gson: Gson
) : BaseRemoteSource(), AuthRemoteSource {
    override fun logout(): Completable {
        return Completable.complete()
    }

    override fun forgotPassword(email: String): Single<Boolean> {
        val emailJson = JsonObject()
            .apply {
                addProperty("email", email)
            }
        val requestBody = getJsonRequestBody(emailJson.toString())

        return baseplateApiServices
            .forgotPassword(requestBody)
            .map { response ->
                response.success
            }
    }

    override fun forgotPasswordCheckCode(email: String, code: String): Single<Boolean> {
        val emailJson = JsonObject()
            .apply {
                addProperty("email", email)
                addProperty("token", code)
            }

        return baseplateApiServices
            .forgotPasswordCheckCode(getJsonRequestBody(emailJson.toString()))
            .map { it.success }
    }

    override fun newPassword(email: String, token: String, password: String, confirmPassword: String): Single<Boolean> {
        val passwordJson = JsonObject()
            .apply {
                addProperty("email", email)
                addProperty("token", token)
                addProperty("password", password)
                addProperty("password_confirmation", confirmPassword)
            }

        return baseplateApiServices
            .resetPassword(getJsonRequestBody(passwordJson.toString()))
            .map { it.success }
    }

    override fun checkEmail(email: String): Single<Boolean> {
        val userMap = hashMapOf<String, String>()
        userMap["email"] = email

        return baseplateApiServices
            .checkEmailIfExists(userMap)
            .map { it.data.isEmailExists }
    }

    override fun updateUserSession(accessToken: AccessToken, userSession: UserSession): Single<UserSession> {
        val json = userSession.toJsonStringExcludeEmpty()
        return baseplateApiServices
            .updateUserInfo(
                accessToken.bearerToken,
                getJsonRequestBody(json)
            )
            .map { it.data }
            .map { UserSessionDTO.toDomain(it) }
    }

    override fun login(username: String, password: String): Single<Map<String, Any>> {
        val userMap = hashMapOf<String, String>()
        userMap["username"] = username
        userMap["password"] = password

        return baseplateApiServices
            .login(userMap)
            .map { UserSessionDTO.mapAuthDataResponse(it) }
    }

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    override fun socialLogin(accessToken: String, accessTokenProvider: String): Single<Map<String, Any>> {
        val map = mapOf(
            "token" to accessToken,
            "provider" to accessTokenProvider
        )

        return baseplateApiServices
            .socialLogin(map)
            .map { UserSessionDTO.mapAuthDataResponse(it) }
    }

    override fun register(
        email: String,
        password: String,
        confirmPassword: String,
        mobileNumber: String,
        firstName: String,
        lastName: String
    ): Single<Map<String, Any>> {
        val registerBody = JsonObject()
            .apply {
                if (email.isNotEmpty()) addProperty("email", email)
                if (firstName.isNotEmpty()) addProperty("first_name", firstName)
                if (lastName.isNotEmpty()) addProperty("last_name", lastName)
                if (password.isNotEmpty()) addProperty("password", password)
                if (confirmPassword.isNotEmpty()) addProperty("password_confirmation", confirmPassword)
                if (mobileNumber.isNotEmpty()) addProperty("phone_number", mobileNumber)
            }

        return baseplateApiServices
            .register(getJsonRequestBody(registerBody.toString()))
            .map { UserSessionDTO.mapAuthDataResponse(it) }
    }

    override fun verifyAccountEmail(accessToken: AccessToken, code: String): Single<Pair<Boolean, UserSession>> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("token", code)
                addProperty("via", "email")
            }
        return baseplateApiServices
            .verifyAccountEmail(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map { UserSessionDTO.toDomain(it.user) }
            .map { Pair(it.emailVerified, it) }
    }

    override fun resendEmailVerificationCode(accessToken: AccessToken): Single<Boolean> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("via", "email")
            }
        return baseplateApiServices
            .resendVerificationCode(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map {
                it.success
            }
    }

    override fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>> {
        val jsonObject = JsonObject()

        if (countryCodes.isNotEmpty()) {
            jsonObject
                .add(
                    "calling_codes",
                    gson.toJsonTree(countryCodes.toList()).asJsonArray
                )
        }

        val countryCode =
            if (countryCodes.isNotEmpty()) {
                baseplateApiServices
                    .getCountryCodes(
                        getJsonRequestBody(jsonObject.toString())
                    )
            } else {
                baseplateApiServices.getAllCountryCodes()
            }

        return countryCode
            .toObservable()
            .flatMapIterable {
                it.data.countries
            }
            .map {
                CountryCode(
                    id = it.id,
                    name = it.name,
                    callingCode = it.callingCode,
                    flag = it.flag.orEmpty()
                )
            }
            .toList()
            .onErrorResumeNext(Single.just(Stubs.countries))
    }
}
