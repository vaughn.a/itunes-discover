package com.appetiser.module.local.features.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.appetiser.module.domain.features.main.models.Media
import com.appetiser.module.local.AppDatabase
import com.appetiser.module.local.features.main.models.DBMedia
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class MediaLocalSourceImpl @Inject constructor(
    private val database: AppDatabase
) : MediaLocalSource {

    override fun saveMedia(media: Media): Single<Media> {
        return Single.create { emitter ->
            val dbMedia = DBMedia.fromDomain(media)
            database
                .mediaDao()
                .insertOrUpdate(dbMedia)

            emitter.onSuccess(media)
        }
    }

    override fun getAll(): Single<List<Media>> {
        return Single.create {
            database
                .mediaDao()
                .getAllMedia()
        }
    }

    override fun getAllLiveData(): LiveData<List<Media>> {
        return Transformations.map(database.mediaDao().getAllMediaLive()) {
            ArrayList<Media>().apply {
                it.forEach {
                    DBMedia.toDomain(it)
                }
            }
        }
    }

    override fun clear(): Completable {
        return Completable.create {
            database.mediaDao().clear()

            it.onComplete()
        }
    }
}
