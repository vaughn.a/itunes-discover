package com.appetiser.module.network.features.main.models

import com.appetiser.module.domain.features.main.models.Media

data class MediaResponseData(
    val results: List<Media>
)
