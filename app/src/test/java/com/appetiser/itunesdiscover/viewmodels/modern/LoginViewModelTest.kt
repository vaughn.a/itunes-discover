package com.appetiser.itunesdiscover.viewmodels.modern

import com.appetiser.itunesdiscover.core.TestSchedulerProvider
import com.appetiser.itunesdiscover.core.TestUtils
import com.appetiser.itunesdiscover.features.auth.modern.login.LoginState
import com.appetiser.itunesdiscover.features.auth.modern.login.LoginViewModel
import com.appetiser.module.data.features.modern.auth.AuthRepository
import com.appetiser.module.data.mapper.modern.UserSessionMapper
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*

class LoginViewModelTest {

    private lateinit var loginViewModel: LoginViewModel

    private val testScheduler = TestScheduler()
    private val schedulers = TestSchedulerProvider(testScheduler)

    private val repository = mock(AuthRepository::class.java)
    private val observer = mock(TestObserver::class.java) as TestObserver<LoginState>

    @Before
    fun setup() {
        loginViewModel = LoginViewModel(repository)
        loginViewModel.schedulers = schedulers
        loginViewModel.state.subscribe(observer)
    }

    @Test
    fun `when user login expect user session email verified`() {
        val email = "test@test.test"
        val password = "password"

        val map = mutableMapOf<String, Any>()
        map[UserSessionMapper.USER_KEY] = TestUtils.userSession

        val expected = LoginState.LoginSuccess(user = TestUtils.userSession)

        `when`(repository.login(email, password)).thenReturn(Single.just(map))

        loginViewModel.login(email, password)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(LoginState.LoginSuccess::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun `when user login expect user session not verified`() {
        val email = "test@test.test"
        val password = "password"

        val map = mutableMapOf<String, Any>()
        map[UserSessionMapper.USER_KEY] = TestUtils.userSessionNotVerified

        val expected = LoginState.UserNotVerified(user = TestUtils.userSessionNotVerified)

        `when`(repository.login(email, password)).thenReturn(Single.just(map))

        loginViewModel.login(email, password)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(LoginState.UserNotVerified::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun `when user login expect error`() {
        val email = "foo@bar.baz"
        val password = "password"

        val error = Throwable("Something went wrong")

        val expected = LoginState.Error(error)

        `when`(repository.login(username = email, password = password)).thenReturn(Single.error(error))

        loginViewModel.login(email, password)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(LoginState.Error::class.java).run {
            verify(observer, atLeast(2)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }
}
