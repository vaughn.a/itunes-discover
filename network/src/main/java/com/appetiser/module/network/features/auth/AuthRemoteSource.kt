package com.appetiser.module.network.features.auth

import com.appetiser.module.domain.features.auth.models.AccessToken
import com.appetiser.module.domain.features.auth.models.CountryCode
import com.appetiser.module.domain.features.auth.models.UserSession
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRemoteSource {
    fun logout(): Completable

    fun forgotPassword(email: String): Single<Boolean>

    fun forgotPasswordCheckCode(email: String, code: String): Single<Boolean>

    fun newPassword(email: String, token: String, password: String, confirmPassword: String): Single<Boolean>

    fun checkEmail(email: String): Single<Boolean>

    fun updateUserSession(accessToken: AccessToken, userSession: UserSession): Single<UserSession>

    fun login(username: String, password: String): Single<Map<String, Any>>

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    fun socialLogin(accessToken: String, accessTokenProvider: String): Single<Map<String, Any>>

    fun register(
        email: String,
        password: String,
        confirmPassword: String,
        mobileNumber: String,
        firstName: String,
        lastName: String
    ): Single<Map<String, Any>>

    /**
     * @return Returns pair of boolean true/false if verification succeeds and user session.
     */
    fun verifyAccountEmail(accessToken: AccessToken, code: String): Single<Pair<Boolean, UserSession>>

    fun resendEmailVerificationCode(accessToken: AccessToken): Single<Boolean>

    fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>>
}
