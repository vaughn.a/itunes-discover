package com.appetiser.itunesdiscover.viewmodels.modern

import com.appetiser.itunesdiscover.core.TestSchedulerProvider
import com.appetiser.itunesdiscover.features.auth.modern.forgotpassword.ForgotPasswordState
import com.appetiser.itunesdiscover.features.auth.modern.forgotpassword.ForgotPasswordViewModel
import com.appetiser.module.data.features.modern.auth.AuthRepository
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.mockito.Mockito

class ForgotPasswordViewModelTest {

    private lateinit var viewModel: ForgotPasswordViewModel

    private val testScheduler = TestScheduler()
    private val schedulers = TestSchedulerProvider(testScheduler)

    private val repository = Mockito.mock(AuthRepository::class.java)
    private val observer = Mockito.mock(TestObserver::class.java) as TestObserver<ForgotPasswordState>

    @Before
    fun setup() {
        viewModel = ForgotPasswordViewModel(repository)
        viewModel.schedulers = schedulers
        viewModel.state.subscribe(observer)
    }
}
