package com.appetiser.module.local.features.auth.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.features.auth.models.AccessToken

@Entity(tableName = DBToken.TOKEN_TABLE_NAME)
data class DBToken(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    var token: String? = "",
    var refresh: String? = "",
    @ColumnInfo(name = "token_type")
    var tokenType: String? = "",
    @ColumnInfo(name = "expires_in")
    var expiresIn: String? = ""
) {
    companion object {
        const val TOKEN_TABLE_NAME = "token"

        fun toDomain(dbToken: DBToken): AccessToken {
            return with(dbToken) {
                AccessToken(
                    token = token,
                    refresh = "",
                    tokenType = tokenType,
                    expiresIn = expiresIn
                )
            }
        }
    }

    constructor() : this(0, "", "", "", "")

    val bearerToken: String get() = "Bearer $token"
}
