package com.appetiser.module.local.features.auth.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.features.auth.models.UserSession

@Entity(tableName = DBUserSession.USER_TABLE_NAME)
data class DBUserSession(
    @PrimaryKey(autoGenerate = true)
    var pk_id: Long? = null,
    @ColumnInfo(name = "full_name")
    var fullName: String? = "",
    @ColumnInfo(name = "first_name")
    var firstName: String? = "",
    @ColumnInfo(name = "last_name")
    var lastName: String? = "",
    var email: String? = "",
    @ColumnInfo(name = "avatar_permanent_url")
    var avatarPermanentUrl: String? = "",
    @ColumnInfo(name = "avatar_permanent_thumb_url")
    var avatarPermanentThumbUrl: String? = "",
    @ColumnInfo(name = "phone_number")
    var phoneNumber: String? = "",
    @ColumnInfo(name = "email_verified")
    var emailVerified: Boolean = false,
    @ColumnInfo(name = "phone_number_verified")
    var phoneNumberVerified: Boolean = false,
    var verified: Boolean = false,
    var uid: String = ""
) {

    companion object {
        const val USER_TABLE_NAME = "user_session"
        const val EMPTY_USER_ID = "empty"

        /**
         * Returns an empty user.
         */
        fun empty(): DBUserSession {
            return DBUserSession(
                fullName = EMPTY_USER_ID
            )
        }

        fun fromDomain(userSession: UserSession): DBUserSession {
            return with(userSession) {
                DBUserSession(
                    fullName = fullName,
                    firstName = firstName,
                    lastName = lastName,
                    email = email,
                    avatarPermanentUrl = avatarPermanentUrl.orEmpty(),
                    avatarPermanentThumbUrl = avatarPermanentThumbUrl.orEmpty(),
                    phoneNumber = phoneNumber,
                    emailVerified = emailVerified,
                    phoneNumberVerified = phoneNumberVerified,
                    verified = verified,
                    uid = id.orEmpty()
                )
            }
        }

        fun toDomain(dbUserSession: DBUserSession): UserSession {
            return with(dbUserSession) {
                UserSession(
                    phoneNumber = phoneNumber.orEmpty(),
                    emailVerified = emailVerified,
                    phoneNumberVerified = phoneNumberVerified,
                    verified = verified,
                    email = email.orEmpty(),
                    lastName = lastName.orEmpty(),
                    firstName = firstName.orEmpty(),
                    fullName = fullName.orEmpty(),
                    avatarPermanentUrl = avatarPermanentUrl.orEmpty(),
                    avatarPermanentThumbUrl = avatarPermanentThumbUrl.orEmpty(),
                    id = uid
                )
            }
        }
    }

    fun isEmptyUser(): Boolean {
        return fullName == EMPTY_USER_ID
    }

    constructor() : this(
        0, "", "", "", "", "",
        "", "", false, false, false, ""
    )
}
