package com.appetiser.itunesdiscover.features.auth.register

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.auth_modern.databinding.ActivityOnboardRegisterStep1Binding
import com.appetiser.itunesdiscover.R
import com.appetiser.itunesdiscover.base.BaseViewModelActivity
import com.appetiser.itunesdiscover.ext.disabledWithAlpha
import com.appetiser.itunesdiscover.ext.enabledWithAlpha
import com.appetiser.itunesdiscover.features.auth.register.verification.RegisterVerificationActivity
import com.appetiser.module.common.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.common.widget.CustomPasswordTransformation
import com.appetiser.module.network.base.response.error.ResponseError
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class RegisterActivity : BaseViewModelActivity<ActivityOnboardRegisterStep1Binding, RegisterViewModel>() {

    companion object {
        fun openActivity(context: Context, email: String) {
            val intent = Intent(context, RegisterActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            context.startActivity(intent)
        }

        const val KEY_EMAIL = "email"
    }

    override fun getLayoutId(): Int = R.layout.activity_onboard_register_step1

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()

        setupViewModels()
        observeInputViews()
        setupData()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViewModels() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    when (it) {
                        is RegisterState.GetEmail -> {
                            binding.etEmail.apply {
                                isEnabled = false
                                setText(it.email)
                            }
                        }

                        is RegisterState.SaveLoginCredentials -> {
                            RegisterVerificationActivity.openActivity(this@RegisterActivity, it.user.email, true)
                            finish()
                        }

                        is RegisterState.GetCountryCode -> {
                        }

                        is RegisterState.Error -> {
                            // show error message
                            ResponseError.getError(it.throwable,
                                ResponseError.ErrorCallback(httpExceptionCallback = {
                                    toast(it)
                                }))
                        }
                        is RegisterState.ShowProgressLoading -> {
                            toast("Sending request")
                        }
                    }
                },
                onError = {
                    Timber.e("Error $it")
                }
            ).apply { disposables.add(this) }
    }

    private fun setupData() {
        binding.etMobile.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {

                    if (binding.btnContinue.isEnabled) {
                        register()
                    }
                    true
                } else {
                    false
                }
            }
        }

        disposables.add(binding.btnContinue.ninjaTap {
            register()
        })
    }

    private fun register() {
        viewModel.register(
            password = binding.etPassword.text.toString(),
            mobileNumber = binding.countryCodePicker.fullNumberWithPlus
        )
    }

    private fun observeInputViews() {
        binding.etPassword.apply {
            transformationMethod = CustomPasswordTransformation()
        }

        binding.countryCodePicker.registerPhoneNumberTextView(binding.etMobile)

        val passwordObservable = binding.etPassword.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() && it.length >= 8 }

        val phoneNumberObservable = binding.etMobile.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() }

        disposables.add(
            Observable.combineLatest(
                passwordObservable,
                phoneNumberObservable,
                BiFunction<Boolean, Boolean, Boolean> { passwordCount, phoneNumberCount ->
                    passwordCount && phoneNumberCount
                })
                .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribe({
                    if (it) {
                        binding.btnContinue.enabledWithAlpha()
                    } else {
                        binding.btnContinue.disabledWithAlpha()
                    }
                }, {
                    Timber.e(it)
                })
        )
    }
}
