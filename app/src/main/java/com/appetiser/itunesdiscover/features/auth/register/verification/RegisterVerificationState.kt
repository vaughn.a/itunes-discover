package com.appetiser.itunesdiscover.features.auth.register.verification

sealed class RegisterVerificationState {

    data class GetEmail(val email: String) : RegisterVerificationState()

    object IndicatorVisible : RegisterVerificationState()

    object IndicatorGone : RegisterVerificationState()

    data class SendCodeSuccess(val email: String, val token: String) : RegisterVerificationState()

    object ResendCodeSuccess : RegisterVerificationState()

    data class Error(val throwable: Throwable) : RegisterVerificationState()

    object ShowProgressLoading : RegisterVerificationState()

    object HideProgressLoading : RegisterVerificationState()
}
