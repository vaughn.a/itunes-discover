package com.appetiser.itunesdiscover.features.auth.forgotpassword

sealed class ForgotPasswordState {

    data class GetEmail(val email: String) : ForgotPasswordState()

    data class Success(val isSuccess: Boolean) : ForgotPasswordState()

    data class Error(val throwable: Throwable) : ForgotPasswordState()

    object ShowProgressLoading : ForgotPasswordState()

    object HideProgressLoading : ForgotPasswordState()
}
