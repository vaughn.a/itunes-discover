package com.appetiser.module.local.features.main.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.features.main.models.DBMedia
import io.reactivex.Single

@Dao
abstract class MediaDao : BaseDao<DBMedia> {

    @Query("SELECT * from ${DBMedia.MEDIA_TABLE_NAME}")
    abstract fun getAllMedia(): Single<List<DBMedia>>

    @Query("DELETE from ${DBMedia.MEDIA_TABLE_NAME}")
    abstract fun clear()

    @Query("SELECT * from ${DBMedia.MEDIA_TABLE_NAME} WHERE track_id = :trackId LIMIT 1")
    abstract fun getMedia(trackId: Int): DBMedia

    @Query("SELECT * from ${DBMedia.MEDIA_TABLE_NAME}")
    abstract fun getAllMediaLive(): LiveData<List<DBMedia>>
}
