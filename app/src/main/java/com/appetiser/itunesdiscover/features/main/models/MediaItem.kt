package com.appetiser.itunesdiscover.features.main.models

import android.view.View
import com.appetiser.itunesdiscover.R
import com.appetiser.itunesdiscover.databinding.ItemMediaBinding
import com.appetiser.module.domain.features.main.models.Media
import com.xwray.groupie.databinding.BindableItem
import com.xwray.groupie.databinding.GroupieViewHolder

class MediaItem(internal val media: Media) : BindableItem<ItemMediaBinding>() {
    override fun getLayout(): Int = R.layout.item_media

    override fun bind(viewBinding: ItemMediaBinding, position: Int) {
        viewBinding.item = media
    }

    override fun createViewHolder(itemView: View): GroupieViewHolder<ItemMediaBinding> {
        return super.createViewHolder(itemView)
    }
}
