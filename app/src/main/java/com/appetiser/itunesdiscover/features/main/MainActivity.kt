package com.appetiser.itunesdiscover.features.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.appetiser.itunesdiscover.R
import com.appetiser.itunesdiscover.base.BaseViewModelActivity
import com.appetiser.itunesdiscover.databinding.ActivityMainBinding
import com.appetiser.itunesdiscover.features.detail.DetailActivity
import com.appetiser.itunesdiscover.features.detail.DetailActivity.Companion.EXTRA_MEDIA_DATA
import com.appetiser.itunesdiscover.features.main.models.MediaItem
import com.appetiser.module.common.toast
import com.appetiser.module.domain.features.main.models.MediaType
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class MainActivity : BaseViewModelActivity<ActivityMainBinding, MainViewModel>() {
    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    MainActivity::class.java
                )
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    private lateinit var adapter: GroupAdapter<GroupieViewHolder>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Init Groupie
        adapter = GroupAdapter()

        setUpViewModelsObserver()
        setupTabLayout()
        setupRecyclerView()
    }

    /**
     * Setup Tablayout with MediaType enum values.
     * @see MediaType
     */
    private fun setupTabLayout() {
        enumValues<MediaType>().forEach {
            binding.tabLayout.addTab(binding.tabLayout.newTab().setText(it.stringValue))
        }

        binding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let {
                    viewModel.changeMainState(it.position, it.text.toString())
                }
            }
        })
    }

    /**
     * Setup recyclerview usingn Groupie
     * @link https://github.com/lisawray/groupie
     */
    private fun setupRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter

        binding.search.setOnEditorActionListener { v, actionId, _ ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_GO -> {
                    v.isFocusableInTouchMode = false
                    v.isFocusable = false
                    v.isFocusableInTouchMode = true
                    v.isFocusable = true

                    viewModel.filterFeedData(
                        binding.search.text.toString(),
                        "Au",
                        viewModel.selectedTabStr.value ?: "all"
                    )
                    true
                }
                else -> false
            }
        }

        adapter.setOnItemClickListener { item, _ ->
            (item as MediaItem).let { mediaItem ->
                val intent = Intent(this, DetailActivity::class.java).apply {
                    putExtra(EXTRA_MEDIA_DATA, Gson().toJson(mediaItem.media))
                }
                Log.e("main intent", Gson().toJson(mediaItem.media))
                startActivity(intent)
            }
        }
    }

    private fun setUpViewModelsObserver() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    handleState(state)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }

        viewModel.selectedTab.observe(
            this,
            Observer { selectedTab ->
                binding.tabLayout.getTabAt(selectedTab)?.let { it.select() }
            }
        )

        viewModel.feed.observe(this, Observer {
            adapter.update(it)
        })
    }

    private fun handleState(state: MainState?) {
        when (state) {
            MainState.ShowProgressLoading -> {
                toast(getString(R.string.sending_request))
            }
            is MainState.MediaTypeByIndex -> {
                viewModel.filterFeedData(binding.search.text.toString(), "Au", state.type)
            }
            is MainState.MediaFeedData -> {
                adapter.update(state.feed)
            }
        }
    }
}
