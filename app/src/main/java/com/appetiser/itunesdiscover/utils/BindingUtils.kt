package com.appetiser.module.domain.utils

import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.appetiser.itunesdiscover.R
import com.bumptech.glide.Glide

object BindingUtils {
    @JvmStatic
    @BindingAdapter("imageUrl")
    fun setImageUrl(view: AppCompatImageView, url: String?) {
        Glide.with(view.context)
            .load(url)
            .placeholder(R.drawable.ic_music_note_black_24dp)
            .into(view)
    }
}
