package com.appetiser.module.network

import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.auth.models.response.AuthDataResponse
import com.appetiser.module.network.features.auth.models.response.CountryCodeResponse
import com.appetiser.module.network.features.auth.models.response.EmailCheckDataResponse
import com.appetiser.module.network.features.auth.models.response.VerifyEmailResponse
import com.appetiser.module.network.features.profile.models.response.ProfileDataResponse
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.*

interface BaseplateApiServices {

    @POST("auth/register")
    fun register(@Body registerBody: RequestBody): Single<AuthDataResponse>

    @FormUrlEncoded
    @POST("auth/login")
    fun login(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<AuthDataResponse>

    @FormUrlEncoded
    @POST("auth/check-email")
    fun checkEmailIfExists(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<EmailCheckDataResponse>

    @POST("auth/verification/verify")
    fun verifyAccountEmail(@Header("Authorization") token: String, @Body registerBody: RequestBody): Single<VerifyEmailResponse>

    @POST("auth/verification/resend")
    fun resendVerificationCode(@Header("Authorization") token: String, @Body registerBody: RequestBody): Single<BaseResponse>

    @FormUrlEncoded
    @POST("auth/social")
    fun socialLogin(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<AuthDataResponse>

    @PUT("auth/profile")
    fun updateUserInfo(
        @Header("Authorization") token: String,
        @Body registerBody: RequestBody
    ): Single<ProfileDataResponse>

    @GET("auth/logout")
    fun logout(@Header("Authorization") token: String): Single<BaseResponse>

    @POST("auth/reset-password")
    fun resetPassword(
        @Body resetPasswordJson: RequestBody
    ): Single<BaseResponse>

    @POST("auth/forgot-password")
    fun forgotPassword(@Body emailJson: RequestBody): Single<BaseResponse>

    @POST("auth/reset-password/check")
    fun forgotPasswordCheckCode(@Body requestBody: RequestBody): Single<BaseResponse>

    /**
     *
     *  getCountryCodes(@Body body: RequestBody)
     *
     *  sample request body
     *   {
     *   "country_ids": [
     *       "608"
     *       ]
     *   }
     *
     * */
    @POST("countries")
    fun getCountryCodes(@Body body: RequestBody): Single<CountryCodeResponse>

    @POST("countries")
    fun getAllCountryCodes(): Single<CountryCodeResponse>
}
