package com.appetiser.module.data.mapper

import com.appetiser.module.domain.features.auth.models.AccessToken
import com.appetiser.module.domain.features.auth.models.UserSession

class UserSessionMapper private constructor() {

    companion object {
        const val USER_KEY = "user"
        const val ACCESS_TOKEN_KEY = "accessToken"
    }
}

fun Map<String, Any>.getUserSession(): UserSession {
    return this[UserSessionMapper.USER_KEY] as UserSession
}

fun Map<String, Any>.getAccessToken(): AccessToken {
    return this[UserSessionMapper.ACCESS_TOKEN_KEY] as AccessToken
}
