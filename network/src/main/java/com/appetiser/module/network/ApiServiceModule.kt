package com.appetiser.module.network

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiServiceModule {

    @Provides
    @Singleton
    fun providesBaseplateApiServices(retrofit: Retrofit): BaseplateApiServices =
        retrofit.create(BaseplateApiServices::class.java)

    @Provides
    @Singleton
    fun providesItunesApiServices(@Named("iTunesApi") retrofit: Retrofit): ItunesApiService =
        retrofit.create(ItunesApiService::class.java)
}
