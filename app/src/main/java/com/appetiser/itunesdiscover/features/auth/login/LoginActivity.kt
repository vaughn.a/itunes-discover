package com.appetiser.itunesdiscover.features.auth.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.inputmethod.EditorInfo
import com.appetiser.auth_modern.databinding.ActivityLoginBinding
import com.appetiser.itunesdiscover.R
import com.appetiser.itunesdiscover.base.BaseViewModelActivity
import com.appetiser.itunesdiscover.ext.disabledWithAlpha
import com.appetiser.itunesdiscover.ext.enableWhen
import com.appetiser.itunesdiscover.ext.enabledWithAlpha
import com.appetiser.itunesdiscover.features.auth.forgotpassword.ForgotPasswordActivity
import com.appetiser.itunesdiscover.features.auth.register.details.UserDetailsActivity
import com.appetiser.itunesdiscover.features.auth.register.verification.RegisterVerificationActivity
import com.appetiser.itunesdiscover.features.main.MainActivity
import com.appetiser.module.common.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.common.widget.CustomPasswordTransformation
import com.appetiser.module.network.base.response.error.ResponseError
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class LoginActivity : BaseViewModelActivity<ActivityLoginBinding, LoginViewModel>() {

    companion object {
        fun openActivity(context: Context, email: String) {
            val intent = Intent(context, LoginActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            context.startActivity(intent)
        }

        const val KEY_EMAIL = "email"
        private const val KEY_PASSWORD = "password"
    }

    override fun getLayoutId(): Int = R.layout.activity_login

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews(savedInstanceState)
        observeInputViews()
        setUpViewModels()
        setupToolbar()
    }

    private fun setUpViews(savedInstanceState: Bundle?) {
        binding.etPassword.apply {
            transformationMethod = CustomPasswordTransformation()
        }

        binding.etPassword.apply {
            setText(savedInstanceState?.getString(KEY_PASSWORD))
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {

                    if (binding.btnContinue.isEnabled) {
                        viewModel.login(binding.etEmail.text.toString(), binding.etPassword.text.toString())
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding.btnContinue.enableWhen(binding.etEmail) {
            Patterns.EMAIL_ADDRESS.matcher(it).matches() && it.isNotEmpty()
        }

        disposables.add(binding.btnContinue.ninjaTap {
            viewModel.login(binding.etEmail.text.toString(), binding.etPassword.text.toString())
        })

        disposables.add(binding.forgotPassword.ninjaTap {
            ForgotPasswordActivity.openActivity(this, binding.etEmail.text.toString())
        })
    }

    private fun observeInputViews() {
        val passwordObservable = binding.etPassword.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() && it.length >= 8 }

        disposables.add(
            passwordObservable
                .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribeBy(onNext = {
                    if (it) {
                        binding.btnContinue.enabledWithAlpha()
                    } else {
                        binding.btnContinue.disabledWithAlpha()
                    }
                }, onError = {
                    Timber.e(it)
                })
        )
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setUpViewModels() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {
                        is LoginState.GetEmail -> {
                            binding.etEmail.apply {
                                setText(state.email)
                            }
                        }
                        is LoginState.NoUserFirstAndLastName -> {
                            UserDetailsActivity.openActivity(this)
                        }
                        is LoginState.LoginSuccess -> {
                            MainActivity.openActivity(this)
                            finishAffinity()
                        }
                        is LoginState.UserNotVerified -> {
                            RegisterVerificationActivity
                                .openActivity(
                                    this,
                                    state.user.email,
                                    false
                                )
                        }
                        is LoginState.Error -> {
                            // show error message
                            ResponseError.getError(
                                state.throwable,
                                ResponseError.ErrorCallback(httpExceptionCallback = {
                                    toast("Error $it")
                                })
                            )
                        }
                        is LoginState.ShowProgressLoading -> {
                            toast("Sending request")
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }
}
