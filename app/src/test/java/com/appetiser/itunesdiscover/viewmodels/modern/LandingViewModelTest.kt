package com.appetiser.itunesdiscover.viewmodels.modern

import android.os.Bundle
import com.appetiser.itunesdiscover.Stubs
import com.appetiser.itunesdiscover.core.TestSchedulerProvider
import com.appetiser.itunesdiscover.features.auth.modern.landing.LandingState
import com.appetiser.itunesdiscover.features.auth.modern.landing.LandingViewModel
import com.appetiser.module.data.features.modern.auth.AuthRepository
import com.appetiser.module.data.mapper.modern.UserSessionMapper
import com.appetiser.module.domain.utils.any
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.capture
import com.appetiser.module.domain.utils.mock
import com.google.gson.Gson
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*
import org.mockito.internal.matchers.apachecommons.ReflectionEquals

class LandingViewModelTest {

    private lateinit var sut: LandingViewModel

    private val authRepository: AuthRepository = mock()
    private val mockBundle: Bundle = mock()

    private val testScheduler = TestScheduler()
    private val schedulers = TestSchedulerProvider(testScheduler)

    private val observer: TestObserver<LandingState> = mock()

    private val userSessionLoggedIn = Stubs.USER_SESSION_LOGGED_IN
    private val userSessionLoggedInButNotOnboarded = Stubs.USER_SESSION_LOGGED_IN_NOT_ONBOARDED
    private val userSessionNotLoggedIn = Stubs.USER_SESSION_NOT_LOGGED_IN

    @Before
    fun setUp() {
        sut = LandingViewModel(authRepository, Gson())
        sut.schedulers = schedulers
        sut.state.subscribe(observer)
    }

    @Test
    fun isFirstTimeUiCreate_ShouldEmitLoggedInState_WhenUserSessionExistsAndOnboarded() {
        val expected = LandingState.UserIsLoggedIn

        `when`(authRepository.getUserSession())
            .thenReturn(Single.just(userSessionLoggedIn))

        sut.isFirstTimeUiCreate(mockBundle)

        testScheduler
            .triggerActions()

        verify(authRepository, times(1)).getUserSession()

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(1)).onNext(capture())
                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun isFirstTimeUiCreate_ShouldEmitLoggedInNotOnboardedState_WhenUserSessionExistsButNotOnboarded() {
        val expected = LandingState.UserIsLoggedInButNotOnboarded

        `when`(authRepository.getUserSession())
            .thenReturn(Single.just(userSessionLoggedInButNotOnboarded))

        sut.isFirstTimeUiCreate(mockBundle)

        testScheduler
            .triggerActions()

        verify(authRepository, times(1)).getUserSession()

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(1)).onNext(capture())
                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun isFirstTimeUiCreate_ShouldNotEmitLoggedInState_WhenUserSessionDoesNotExist() {
        `when`(authRepository.getUserSession())
            .thenReturn(Single.just(userSessionNotLoggedIn))

        sut.isFirstTimeUiCreate(mockBundle)

        testScheduler
            .triggerActions()

        verify(authRepository, times(1)).getUserSession()
        verify(observer, times(0)).onNext(ArgumentMatchers.any())
    }

    @Test
    fun onFacebookLogin_ShouldReturnLoginSuccess_WhenUserOnboardingDetailsAreFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.FACEBOOK
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccess
        val map = mapOf(UserSessionMapper.USER_KEY to userSessionLoggedIn)

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(map))

        sut.onFacebookLogin(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onFacebookLogin_ShouldReturnLoginSuccessButNotOnboarded_WhenUserOnboardingDetailsAreNotFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.FACEBOOK
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccessButNotOnboarded
        val map = mapOf(UserSessionMapper.USER_KEY to userSessionLoggedInButNotOnboarded)

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(map))

        sut.onFacebookLogin(accessToken)
        testScheduler.triggerActions()

        argumentCaptor<String>()
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onFacebookLogin_ShouldReturnError_WhenErrorOccurs() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.FACEBOOK
        val exception = Exception("Mock exception")
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.Error(exception)

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.error(exception))

        sut.onFacebookLogin(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert
                    .assertTrue(ReflectionEquals(expectedState3).matches(allValues[2]))
            }
    }

    @Test
    fun onGoogleSignIn_ShouldReturnLoginSuccess_WhenUserOnboardingDetailsAreFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.GOOGLE
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccess
        val map = mapOf(UserSessionMapper.USER_KEY to userSessionLoggedIn)

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(map))

        sut.onGoogleSignIn(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onGoogleSignIn_ShouldReturnLoginSuccessButNotOnboarded_WhenUserOnboardingDetailsAreNotFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.GOOGLE
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccessButNotOnboarded
        val map = mapOf(UserSessionMapper.USER_KEY to userSessionLoggedInButNotOnboarded)

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(map))

        sut.onGoogleSignIn(accessToken)
        testScheduler.triggerActions()

        argumentCaptor<String>()
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onGoogleSignIn_ShouldReturnError_WhenErrorOccurs() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.GOOGLE
        val exception = Exception("Mock exception")
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.Error(exception)

        `when`(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.error(exception))

        sut.onGoogleSignIn(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert
                    .assertTrue(ReflectionEquals(expectedState3).matches(allValues[2]))
            }
    }
}
