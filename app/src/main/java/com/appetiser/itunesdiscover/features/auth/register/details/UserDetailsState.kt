package com.appetiser.itunesdiscover.features.auth.register.details

import com.appetiser.module.domain.features.auth.models.UserSession

sealed class UserDetailsState {

    data class UserDetailsUpdated(val userSession: com.appetiser.module.domain.features.auth.models.UserSession) : UserDetailsState()

    data class Error(val throwable: Throwable) : UserDetailsState()

    object ShowProgressLoading : UserDetailsState()

    object HideProgressLoading : UserDetailsState()
}
