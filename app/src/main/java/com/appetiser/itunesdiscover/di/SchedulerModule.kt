package com.appetiser.itunesdiscover.di

import com.appetiser.itunesdiscover.utils.schedulers.BaseSchedulerProvider
import com.appetiser.itunesdiscover.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    fun providesSchedulerSource(): BaseSchedulerProvider =
            SchedulerProvider.getInstance()
}
