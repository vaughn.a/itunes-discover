package com.appetiser.itunesdiscover.di

import android.content.Context
import com.appetiser.itunesdiscover.ItunesdiscoverApplication
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Singleton
    @Binds
    abstract fun providesApplicationContext(app: ItunesdiscoverApplication): Context
}
