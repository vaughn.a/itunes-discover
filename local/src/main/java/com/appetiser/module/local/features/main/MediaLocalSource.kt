package com.appetiser.module.local.features.main

import androidx.lifecycle.LiveData
import com.appetiser.module.domain.features.main.models.Media
import io.reactivex.Completable
import io.reactivex.Single

interface MediaLocalSource {

    fun saveMedia(media: Media): Single<Media>
    fun getAll(): Single<List<Media>>
    fun getAllLiveData(): LiveData<List<Media>>
    fun clear(): Completable
}
