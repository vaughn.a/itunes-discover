package com.appetiser.itunesdiscover.features.auth.forgotpassword.verification

import android.os.Bundle
import com.appetiser.itunesdiscover.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class ForgotPasswordVerificationViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    private lateinit var email: String

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        email = bundle?.getString(ForgotPasswordVerificationActivity.KEY_EMAIL, "")!!
        disposables.add(Observable.just(email)
            .observeOn(schedulers.ui())
            .subscribeBy(onNext = { _state.onNext(ForgotPasswordVerificationState.GetEmail(it)) }))
    }

    private val _state by lazy {
        PublishSubject.create<ForgotPasswordVerificationState>()
    }

    val state: Observable<ForgotPasswordVerificationState> = _state

    fun resendCode() {
        disposables.add(repository.forgotPassword(email)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(ForgotPasswordVerificationState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(ForgotPasswordVerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(ForgotPasswordVerificationState.HideProgressLoading)
            }
            .subscribeBy(onSuccess = {
                _state.onNext(ForgotPasswordVerificationState.ResendTokenSuccess)
            }, onError = {
                _state.onNext(ForgotPasswordVerificationState.Error(it))
            }))
    }

    fun sendToken(token: String) {
        disposables.add(repository.forgotPasswordCheckCode(email, token)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(ForgotPasswordVerificationState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(ForgotPasswordVerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(ForgotPasswordVerificationState.HideProgressLoading)
            }
            .subscribeBy(onSuccess = {
                _state.onNext(ForgotPasswordVerificationState.ForgotPasswordSuccess(email, token))
            }, onError = {
                _state.onNext(ForgotPasswordVerificationState.Error(it))
            }))
    }
}
