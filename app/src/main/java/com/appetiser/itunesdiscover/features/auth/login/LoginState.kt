package com.appetiser.itunesdiscover.features.auth.login

import com.appetiser.module.domain.features.auth.models.UserSession

sealed class LoginState {

    data class GetEmail(val email: String) : LoginState()

    data class LoginSuccess(val user: UserSession) : LoginState()

    data class UserNotVerified(val user: UserSession) : LoginState()

    data class Error(val throwable: Throwable) : LoginState()

    object NoUserFirstAndLastName : LoginState()

    object ShowProgressLoading : LoginState()

    object HideProgressLoading : LoginState()
}
