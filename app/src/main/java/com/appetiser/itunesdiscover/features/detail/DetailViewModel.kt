package com.appetiser.itunesdiscover.features.detail

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.itunesdiscover.base.BaseViewModel
import com.appetiser.module.data.features.main.MediaRepository
import com.appetiser.module.domain.features.main.models.Media
import com.google.gson.Gson
import javax.inject.Inject

class DetailViewModel @Inject constructor(
    private val repo: MediaRepository
) : BaseViewModel() {

    private val _media = MutableLiveData<Media>()
    val media: LiveData<Media> = _media

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        // Retrieve media data from cache from intent
        bundle?.getString(DetailActivity.EXTRA_MEDIA_DATA, "{}")?.let {
            Log.e("asd", it)
            _media.value = Gson().fromJson(it, Media::class.java)
        }
    }
}
