package com.appetiser.module.network

import com.appetiser.module.network.features.main.models.MediaDataResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ItunesApiService {

    @GET("/search")
    fun getAll(
        @Query("term", encoded = true) term: String,
        @Query("country") country: String,
        @Query("media") media: String
    ): Single<MediaDataResponse>
}
