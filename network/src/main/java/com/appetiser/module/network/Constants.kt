package com.appetiser.module.network

const val AUTH_BASE_URL = "http://128.199.193.26"
const val API = "api"
const val VERSION = "v1"
const val ENDPOINT_FORMAT = "%s/%s/%s/"

const val ITUNES_BASE_URL = "https://itunes.apple.com"
