package com.appetiser.itunesdiscover.features.auth.forgotpassword.verification

sealed class ForgotPasswordVerificationState {

    data class GetEmail(val email: String) : ForgotPasswordVerificationState()

    object ResendTokenSuccess : ForgotPasswordVerificationState()

    data class ForgotPasswordSuccess(val email: String, val token: String) : ForgotPasswordVerificationState()

    data class Error(val throwable: Throwable) : ForgotPasswordVerificationState()

    object ShowProgressLoading : ForgotPasswordVerificationState()

    object HideProgressLoading : ForgotPasswordVerificationState()
}
