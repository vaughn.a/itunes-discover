package com.appetiser.itunesdiscover.features.auth.register

import com.appetiser.module.domain.features.auth.models.CountryCode
import com.appetiser.module.domain.features.auth.models.UserSession

sealed class RegisterState {

    data class GetEmail(val email: String) : RegisterState()

    data class SaveLoginCredentials(val user: UserSession) : RegisterState()

    data class GetCountryCode(val countryCode: List<CountryCode>) : RegisterState()

    data class Error(val throwable: Throwable) : RegisterState()

    object ShowProgressLoading : RegisterState()

    object HideProgressLoading : RegisterState()
}
