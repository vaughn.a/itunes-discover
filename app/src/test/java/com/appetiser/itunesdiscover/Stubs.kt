package com.appetiser.itunesdiscover

import com.appetiser.module.domain.features.auth.models.UserSession

object Stubs {
    val USER_SESSION_LOGGED_IN = UserSession(
        id = "1231232132123",
        fullName = "Jose Mari Chan",
        firstName = "Jose Mari",
        lastName = "Chan",
        email = "josemarichan@gmail.com",
        avatarPermanentThumbUrl = "http://www.google.com/",
        avatarPermanentUrl = "http://www.google.com/",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true
    )

    val USER_SESSION_LOGGED_IN_NOT_ONBOARDED = UserSession(
        id = "1231232132123",
        fullName = "",
        firstName = "",
        lastName = "",
        email = "",
        avatarPermanentThumbUrl = "/",
        avatarPermanentUrl = "",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true
    )

    val USER_SESSION_NOT_LOGGED_IN = UserSession.empty()
}
