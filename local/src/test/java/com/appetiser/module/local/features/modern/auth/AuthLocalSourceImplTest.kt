package com.appetiser.module.local.features.modern.auth

import androidx.room.EmptyResultSetException
import com.appetiser.module.domain.features.auth.models.AccessToken
import com.appetiser.module.domain.features.auth.models.UserSession
import com.appetiser.module.local.features.modern.AppDatabase
import com.appetiser.module.local.features.modern.Stubs
import com.appetiser.module.local.features.modern.auth.dao.TokenDao
import com.appetiser.module.local.features.modern.auth.dao.UserDao
import com.appetiser.module.local.features.modern.auth.models.DBToken
import com.appetiser.module.local.features.modern.auth.models.DBUserSession
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class AuthLocalSourceImplTest {

    private val appDatabase: AppDatabase = mock(AppDatabase::class.java)
    private val userDao: UserDao = mock(UserDao::class.java)
    private val tokenDao: TokenDao = mock(TokenDao::class.java)
    private val stubUserSession = Stubs.USER_SESSION
    private val stubDbUserSession = Stubs.DB_USER_SESSION

    private lateinit var sut: AuthLocalSource

    @Before
    fun setUp() {
        sut = AuthLocalSourceImpl(appDatabase)
    }

    @Test
    fun saveCredentials_ShouldLogoutPreviousUser() {
        mockUserDao()

        testSaveCredentials()

        verify(userDao, times(1)).logoutUser()
    }

    @Test
    fun saveCredentials_ShouldInsertNewUserSessionToDatabase() {
        mockUserDao()

        testSaveCredentials()

        verify(userDao, times(1))
            .insertOrUpdate(DBUserSession.fromDomain(stubUserSession))
    }

    @Test
    fun saveCredentials_ShouldEmitUserArgument() {
        val expected = stubUserSession

        mockUserDao()

        testSaveCredentials()
            .assertValue {
                it == expected
            }
            .assertComplete()
    }

    @Test
    fun saveToken_ShouldInsertTokenToDatabase() {
        val token = Stubs.TOKEN

        mockTokenDao()

        sut
            .saveToken(token)
            .test()
            .assertComplete()

        verify(tokenDao, times(1))
            .insert(DBToken(token = token))
    }

    @Test
    fun saveToken_ShouldReturnDBTokenWithTokenArgument() {
        val token = Stubs.TOKEN
        val expected = AccessToken(token)

        mockTokenDao()

        sut
            .saveToken(token)
            .test()
            .assertComplete()
            .assertValue { it == expected }
    }

    @Test
    fun getUser_ShouldReturnEmptyUser_WhenQueryIsEmpty() {
        val expected = Stubs.EMPTY_DB_USER_SESSION.fullName

        mockUserDao()

        `when`(userDao.getUserInfo())
            .thenReturn(
                Single.error(EmptyResultSetException("Empty query."))
            )

        sut
            .getUserSession()
            .test()
            .assertComplete()
            .assertValue { it.fullName == expected }
    }

    @Test
    fun getUser_ShouldMapDbUserSessionToDomainUserSession() {
        val expected = DBUserSession.toDomain(stubDbUserSession)

        mockUserDao()

        `when`(userDao.getUserInfo()).thenReturn(Single.just(stubDbUserSession))

        sut
            .getUserSession()
            .test()
            .assertComplete()
            .assertValue { it == expected }
    }

    @Test
    fun getAccessToken_ShouldMapDbTokenToDomainAccessToken() {
        val token = Stubs.TOKEN
        val expected = AccessToken(token = token)

        mockTokenDao()

        `when`(tokenDao.getToken()).thenReturn(Single.just(DBToken(token = token)))

        sut
            .getAccessToken()
            .test()
            .assertComplete()
            .assertValue { it == expected }
    }

    @Test
    fun logout_ShouldLogoutUserAndToken() {
        mockUserDao()
        mockTokenDao()

        sut
            .logout()
            .test()
            .assertComplete()

        verify(userDao, times(1)).logoutUser()
        verify(tokenDao, times(1)).logoutToken()
    }

    private fun testSaveCredentials(): TestObserver<UserSession> {
        return sut
            .saveCredentials(stubUserSession)
            .test()
            .assertComplete()
    }

    private fun mockTokenDao() {
        `when`(appDatabase.tokenDao()).thenReturn(tokenDao)
    }

    private fun mockUserDao() {
        `when`(appDatabase.userSessionDao()).thenReturn(userDao)
    }
}
