package com.appetiser.module.local.features.auth

import com.appetiser.module.domain.features.auth.models.AccessToken
import com.appetiser.module.domain.features.auth.models.UserSession
import io.reactivex.Completable
import io.reactivex.Single

interface AuthLocalSource {
    fun saveCredentials(user: UserSession): Single<UserSession>

    fun saveToken(token: String): Single<AccessToken>

    fun getUserSession(): Single<UserSession>

    fun getAccessToken(): Single<AccessToken>

    fun logout(): Completable
}
