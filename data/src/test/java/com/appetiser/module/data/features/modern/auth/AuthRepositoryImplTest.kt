package com.appetiser.module.data.features.modern.auth

import com.appetiser.module.data.features.modern.Stubs.email
import com.appetiser.module.data.features.modern.Stubs.firstName
import com.appetiser.module.data.features.modern.Stubs.lastName
import com.appetiser.module.data.features.modern.Stubs.mobileNumber
import com.appetiser.module.data.features.modern.Stubs.password
import com.appetiser.module.data.features.modern.Stubs.passwordConfirmation
import com.appetiser.module.data.features.modern.Stubs.userSession
import com.appetiser.module.data.mapper.modern.UserSessionMapper
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Test
import org.mockito.Mockito

class AuthRepositoryImplTest {
    private val repository = Mockito.mock(AuthRepository::class.java)

    @Test
    fun `when user login with email and password expect login success`() {
        val email = "foo@bar.baz"
        val password = "password"
        val map = mutableMapOf<String, Any>()
        map[UserSessionMapper.USER_KEY] = userSession
        Mockito.`when`(repository.login(email, password)).thenReturn(Single.just(map))
        repository.login(email, password)
            .test()
            .assertValue(map)
            .dispose()
    }

    @Test
    fun `when user login with wrong email or password expect error`() {
        val email = "foo@bar.baz"
        val password = "password"
        val error = Throwable("Invalid Credentials")
        Mockito.`when`(repository.login(email, password)).thenReturn(Single.error(error))
        repository.login(email, password)
            .test()
            .assertError(error)
            .dispose()
    }

    @Test
    fun `when user sign up expect success`() {
        val expected = mutableMapOf<String, Any>()
        expected[UserSessionMapper.USER_KEY] = userSession

        Mockito.`when`(
            repository.register(
                email = email,
                password = password,
                confirmPassword = passwordConfirmation,
                firstName = firstName,
                lastName = lastName,
                mobileNumber = mobileNumber
            )
        ).thenReturn(Single.just(expected))
        repository.register(
            email = email,
            password = password,
            confirmPassword = passwordConfirmation,
            firstName = firstName,
            lastName = lastName,
            mobileNumber = mobileNumber)
            .test()
            .assertValue(expected)
            .dispose()
    }

    @Test
    fun `when user sign up expect error`() {
        val error = Throwable("Something went wrong")

        Mockito.`when`(
            repository.register(
                email = email,
                password = password,
                confirmPassword = passwordConfirmation,
                firstName = firstName,
                lastName = lastName,
                mobileNumber = mobileNumber
            )
        ).thenReturn(Single.error(error))

        repository.register(
            email = email,
            password = password,
            confirmPassword = passwordConfirmation,
            firstName = firstName,
            lastName = lastName,
            mobileNumber = mobileNumber)
            .test()
            .assertError(error)
            .dispose()
    }

    @Test
    fun `when get user session expect user details`() {
        val expected = userSession

        Mockito.`when`(repository.getUserSession()).thenReturn(Single.just(userSession))

        repository.getUserSession()
            .test()
            .assertValue(expected)
            .dispose()
    }

    @Test
    fun `when get user session expect error`() {
        val error = Throwable("Something went wrong")

        Mockito.`when`(repository.getUserSession()).thenReturn(Single.error(error))

        repository.getUserSession()
            .test()
            .assertError(error)
            .dispose()
    }

    @Test
    fun `when user forgot password expect success`() {
        val expected = true
        val email = "foo@bar.baz"

        Mockito.`when`(repository.forgotPassword(email)).thenReturn(Single.just(true))

        repository.forgotPassword(email)
            .test()
            .assertValue(expected)
            .dispose()
    }

    @Test
    fun `when user forgot password expect error`() {
        val error = Throwable("Something when wrong")
        val email = "foo@bar.baz"

        Mockito.`when`(repository.forgotPassword(email)).thenReturn(Single.error(error))

        repository.forgotPassword(email)
            .test()
            .assertError(error)
            .dispose()
    }

    @Test
    fun `when user logout expect success`() {
        Mockito.`when`(repository.logout())
            .thenReturn(Completable.complete())
        repository.logout()
            .test()
            .assertComplete()
    }
}
