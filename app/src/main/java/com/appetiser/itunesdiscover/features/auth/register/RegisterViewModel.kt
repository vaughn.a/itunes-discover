package com.appetiser.itunesdiscover.features.auth.register

import android.app.Application
import android.os.Bundle
import com.appetiser.itunesdiscover.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.mapper.getUserSession
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class RegisterViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val app: Application
) : BaseViewModel() {

    private lateinit var email: String

    private val _state by lazy {
        PublishSubject.create<RegisterState>()
    }

    val state: Observable<RegisterState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        email = bundle?.getString(RegisterActivity.KEY_EMAIL, "")!!
        disposables.add(Observable.just(email)
            .observeOn(schedulers.ui())
            .subscribeBy(onNext = { _state.onNext(RegisterState.GetEmail(it)) }))
    }

    fun register(password: String, mobileNumber: String) {
        disposables.add(repository.register(
            email = email,
            password = password,
            confirmPassword = password,
            mobileNumber = mobileNumber,
            firstName = "",
            lastName = "")
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(RegisterState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(RegisterState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(RegisterState.HideProgressLoading)
            }
            .subscribeBy(onSuccess = {
                val user = it.getUserSession()

                if (user.id.orEmpty().isNotEmpty()) {
                    _state.onNext(RegisterState.SaveLoginCredentials(user))
                }
            }, onError = {
                _state.onNext(RegisterState.Error(it))
            })
        )
    }

//    fun fetchCountryCode() {
//        val isCountryCodeValid = countryCode > 1
//        val list = mutableListOf<Int>()
//
//        if (isCountryCodeValid) {
//            list.add(countryCode)
//        }
//
//        disposables.add(
//            repository.getCountryCode(list)
//                .subscribeOn(schedulers.io())
//                .observeOn(schedulers.ui())
//                .subscribeBy(
//                    onSuccess = {
//                        _state.onNext(RegisterState.GetCountryCode(it))
//                    },
//                    onError = {
//                        Timber.e("Error: $it")
//                    })
//        )
//    }
}
