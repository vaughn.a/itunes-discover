package com.appetiser.module.network.features.main

import com.appetiser.module.network.ItunesApiService
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.features.main.models.MediaDataResponse
import com.google.gson.Gson
import io.reactivex.Single
import javax.inject.Inject

class MediaRemoteSourceImpl @Inject constructor(
    private val itunesApiService: ItunesApiService,
    private val gson: Gson
) : BaseRemoteSource(), MediaRemoteSource {

    override fun getAll(
        term: String,
        country: String,
        media: String
    ): Single<MediaDataResponse> {
        return itunesApiService.getAll(term, country, media)
    }
}
