package com.appetiser.module.local.features.auth.dao

import androidx.room.Dao
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.features.auth.models.DBUserSession
import io.reactivex.Single

@Dao
abstract class UserDao : BaseDao<DBUserSession> {

    @Query("SELECT * FROM ${DBUserSession.USER_TABLE_NAME} LIMIT 1")
    abstract fun getUserInfo(): Single<DBUserSession>

    @Query("DELETE FROM ${DBUserSession.USER_TABLE_NAME}")
    abstract fun logoutUser()

    @Query("UPDATE ${DBUserSession.USER_TABLE_NAME} SET full_name = :fullName, avatar_permanent_url = :photoUrl WHERE email =:email")
    abstract fun updateUser(fullName: String?, email: String?, photoUrl: String?): Int
}
