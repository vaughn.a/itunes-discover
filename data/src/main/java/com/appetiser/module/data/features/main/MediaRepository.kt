package com.appetiser.module.data.features.main

import androidx.lifecycle.LiveData
import com.appetiser.module.domain.features.main.models.Media
import io.reactivex.Single

interface MediaRepository {

    fun saveMediaFeed(media: List<Media>): Single<List<Media>>

    fun getMediaFromRemote(
        term: String,
        country: String,
        media: String
    ): Single<List<Media>>

    fun getMediaFeed(): Single<List<Media>>
    fun getLiveMediaFeed(): LiveData<List<Media>>
}
