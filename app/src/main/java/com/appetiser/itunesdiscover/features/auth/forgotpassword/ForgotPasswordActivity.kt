package com.appetiser.itunesdiscover.features.auth.forgotpassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.inputmethod.EditorInfo
import com.appetiser.auth_modern.databinding.ActivityForgotPasswordBinding
import com.appetiser.itunesdiscover.R
import com.appetiser.itunesdiscover.base.BaseViewModelActivity
import com.appetiser.itunesdiscover.ext.enableWithAplhaWhen
import com.appetiser.itunesdiscover.features.auth.forgotpassword.verification.ForgotPasswordVerificationActivity
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.network.base.response.error.ResponseError
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class ForgotPasswordActivity : BaseViewModelActivity<ActivityForgotPasswordBinding, ForgotPasswordViewModel>() {

    companion object {
        const val KEY_EMAIL = "email"

        fun openActivity(context: Context, email: String) {
            val intent = Intent(context, ForgotPasswordActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_forgot_password

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViews()
        setupToolbar()
        setupViewModels()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViews() {
        binding.etEmail.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {

                    if (binding.btnContinue.isEnabled) {
                        viewModel.forgotPassword(binding.etEmail.text.toString())
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding.btnContinue.enableWithAplhaWhen(binding.etEmail) { Patterns.EMAIL_ADDRESS.matcher(it).matches() }

        disposables.add(binding.btnContinue.ninjaTap {
            viewModel.forgotPassword(binding.etEmail.text.toString())
        })
    }

    private fun setupViewModels() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {
                        is ForgotPasswordState.GetEmail -> {
                            binding.etEmail.apply {
                                setText(state.email)
                            }
                        }

                        is ForgotPasswordState.Success -> {
                            ForgotPasswordVerificationActivity.openActivity(this, binding.etEmail.text.toString())
                        }

                        is ForgotPasswordState.Error -> {
                            ResponseError.getError(
                                state.throwable,
                                ResponseError.ErrorCallback(httpExceptionCallback = {
                                    toast("Error $it")
                                })
                            )
                        }
                        is ForgotPasswordState.ShowProgressLoading -> {
                            toast("Sending request")
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }
}
