package com.appetiser.module.data.features.modern

import com.appetiser.module.domain.features.auth.models.UserSession

object Stubs {
    val email = "foo@bar.baz"
    val password = "password"
    val passwordConfirmation = "password"
    val firstName = "foo"
    val lastName = "bar"
    val mobileNumber = "09177707257"

    val userSession = UserSession(
        id = "1111",
        fullName = "Test",
        firstName = "Test",
        lastName = "Test",
        email = "test@test.test",
        avatarPermanentThumbUrl = "http://photo",
        avatarPermanentUrl = "http://photo",
        verified = true,
        emailVerified = true,
        phoneNumber = "09177707257",
        phoneNumberVerified = false
    )

    val userSessionNotVerified = UserSession(
        id = "1111",
        fullName = "Test",
        firstName = "Test",
        lastName = "Test",
        email = "test@test.test",
        avatarPermanentThumbUrl = "http://photo",
        avatarPermanentUrl = "http://photo",
        verified = false,
        emailVerified = false,
        phoneNumber = "09177707257",
        phoneNumberVerified = false
    )
}
