package com.appetiser.itunesdiscover.features.auth.register.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.auth_modern.databinding.ActivityRegisterUserDetailsBinding
import com.appetiser.itunesdiscover.R
import com.appetiser.itunesdiscover.base.BaseViewModelActivity
import com.appetiser.itunesdiscover.ext.disabledWithAlpha
import com.appetiser.itunesdiscover.ext.enabledWithAlpha
import com.appetiser.itunesdiscover.features.main.MainActivity
import com.appetiser.module.common.*
import com.appetiser.module.network.base.response.error.ResponseError
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class UserDetailsActivity : BaseViewModelActivity<ActivityRegisterUserDetailsBinding, UserDetailsViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, UserDetailsActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_register_user_details

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setUpViews()
        observeInputViews()
        setupViewModel()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setUpViews() {
        binding.etLastName.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {

                    if (binding.btnCreateAccount.isEnabled) {
                        viewModel.sendUserDetails(binding.etFirstName.text.toString(), binding.etLastName.text.toString())
                    }
                    true
                } else {
                    false
                }
            }
        }

        disposables.add(binding.btnCreateAccount.ninjaTap {
            viewModel.sendUserDetails(binding.etFirstName.text.toString(), binding.etLastName.text.toString())
        })
    }

    private fun observeInputViews() {
        binding.etFirstName.capitalizeFirstLetter()
        binding.etLastName.capitalizeFirstLetter()

        val firstNameObservable = binding.etFirstName.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() }

        val lastNameObservable = binding.etLastName.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() }

        disposables.add(
            Observable.combineLatest(
                firstNameObservable,
                lastNameObservable,
                BiFunction<Boolean, Boolean, Boolean> { firstName, lastName ->
                    firstName && lastName
                })
                .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it) {
                            binding.btnCreateAccount.enabledWithAlpha()
                        } else {
                            binding.btnCreateAccount.disabledWithAlpha()
                        }
                    },
                    onError = {
                        Timber.e("Error $it")
                    }
                )
        )
    }

    private fun setupViewModel() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    when (it) {

                        is UserDetailsState.UserDetailsUpdated -> {
                            MainActivity.openActivity(this)
                            finishAffinity()
                        }

                        is UserDetailsState.Error -> {
                            // show error message
                            ResponseError.getError(it.throwable,
                                ResponseError.ErrorCallback(httpExceptionCallback = {
                                    toast(it)
                                }))
                        }
                        is UserDetailsState.ShowProgressLoading -> {
                            toast("Sending request")
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            ).apply { disposables.add(this) }
    }
}
