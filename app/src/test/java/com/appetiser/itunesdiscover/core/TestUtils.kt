package com.appetiser.itunesdiscover.core

import com.appetiser.module.domain.features.auth.models.UserSession
import retrofit2.Response
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody

class TestUtils {

    companion object {
        val userSession = UserSession(
            id = "1111",
            fullName = "Test",
            firstName = "Test",
            lastName = "Test",
            email = "test@test.test",
            avatarPermanentThumbUrl = "http://photo",
            avatarPermanentUrl = "http://photo",
            verified = true,
            emailVerified = true,
            phoneNumber = "09177707257",
            phoneNumberVerified = false
        )

        val userSessionNotVerified = UserSession(
            id = "1111",
            fullName = "Test",
            firstName = "Test",
            lastName = "Test",
            email = "test@test.test",
            avatarPermanentThumbUrl = "http://photo",
            avatarPermanentUrl = "http://photo",
            verified = false,
            emailVerified = false,
            phoneNumber = "09177707257",
            phoneNumberVerified = false
        )

        fun buildErrorResponseCheckEmail(): Response<String> {
            val sdf = "{\n" +
                "    \"message\": \"We couldn't find any records that matches your email.\",\n" +
                "    \"error_code\": \"EMAIL_NOT_FOUND\",\n" +
                "    \"http_status\": 404,\n" +
                "    \"success\": false\n" +
                "}"

            return Response.error<String>(404, sdf.toResponseBody("application/json".toMediaTypeOrNull()))
        }
    }
}
