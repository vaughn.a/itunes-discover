package com.appetiser.itunesdiscover.features.auth.register.verification

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import com.appetiser.auth_modern.databinding.ActivityEmailVerificationCodeBinding
import com.appetiser.itunesdiscover.R
import com.appetiser.itunesdiscover.base.BaseViewModelActivity
import com.appetiser.itunesdiscover.ext.enableWithAplhaWhen
import com.appetiser.itunesdiscover.features.auth.register.details.UserDetailsActivity
import com.appetiser.module.common.*
import com.appetiser.module.network.base.response.error.ResponseError
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class RegisterVerificationActivity : BaseViewModelActivity<ActivityEmailVerificationCodeBinding, RegisterVerificationViewModel>() {

    companion object {

        fun openActivity(context: Context, email: String, stepIndicatorVisible: Boolean) {
            val intent = Intent(context, RegisterVerificationActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            intent.putExtra(KEY_STEP_INDICATOR, stepIndicatorVisible)
            context.startActivity(intent)
        }

        const val KEY_EMAIL = "email"
        const val KEY_STEP_INDICATOR = "indicator"
    }

    override fun getLayoutId(): Int = R.layout.activity_email_verification_code

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViews()
        setupToolbar()
        setupViewModels()
        observeInputViews()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViews() {
        binding.inputCode.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    if (binding.btnContinue.isEnabled) {
                        viewModel.sendToken(binding.inputCode.text.toString())
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding.btnContinue.enableWithAplhaWhen(binding.inputCode) { it.isNotEmpty() && it.length >= 5 }

        disposables.add(binding.btnContinue.ninjaTap {
            viewModel.sendToken(binding.inputCode.text.toString())
        })

        disposables.add(binding.noCode.ninjaTap {
            viewModel.resendToken()
        })
    }

    private fun observeInputViews() {
        binding.inputCode.textChangeEvents()
            .skipInitialValue()
            .observeOn(scheduler.ui())
            .map { it.text }
            .map {
                it.isNotEmpty() && it.length >= 5
            }
            .subscribeBy(onNext = {
                if (it) {
                    viewModel.sendToken(binding.inputCode.text.toString())
                }
            }, onError = {
                Timber.e(it)
            }).apply { disposables.add(this) }
    }

    private fun setupViewModels() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    when (it) {
                        is RegisterVerificationState.IndicatorVisible -> {
                            binding.indicatorContainer.setVisible(true)
                        }
                        is RegisterVerificationState.IndicatorGone -> {
                            binding.indicatorContainer.setVisible(false)
                        }
                        is RegisterVerificationState.GetEmail -> {
                            binding.onboardingWelcomeBack.movementMethod = LinkMovementMethod.getInstance()
                            binding.onboardingWelcomeBack.text = getString(R.string.verification_code_description, it.email)
                                .spannableString(
                                    this,
                                    null,
                                    R.font.inter_medium,
                                    ContextCompat.getColor(this, R.color.colorPrimaryDark),
                                    it.email,
                                    clickable = {
                                    }
                                )
                        }

                        is RegisterVerificationState.ResendCodeSuccess -> {
                            toast("Request new code sent!")
                        }

                        is RegisterVerificationState.SendCodeSuccess -> {
                            UserDetailsActivity.openActivity(this)
                            finish()
                        }

                        is RegisterVerificationState.Error -> {
                            ResponseError.getError(it.throwable,
                                ResponseError.ErrorCallback(httpExceptionCallback = {
                                    toast(it)
                                }))
                        }

                        is RegisterVerificationState.ShowProgressLoading -> {
                            toast("Sending request")
                        }

                        is RegisterVerificationState.HideProgressLoading -> {
                        }
                    }
                },
                onError = {
                    Timber.e("Error $it")
                }
            )
            .apply {
                disposables.add(this)
            }
    }
}
