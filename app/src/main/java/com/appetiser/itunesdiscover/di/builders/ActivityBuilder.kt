package com.appetiser.itunesdiscover.di.builders

import com.appetiser.itunesdiscover.di.scopes.ActivityScope
import com.appetiser.itunesdiscover.features.auth.AuthRepositoryModule
import com.appetiser.itunesdiscover.features.auth.emailcheck.EmailCheckActivity
import com.appetiser.itunesdiscover.features.auth.forgotpassword.ForgotPasswordActivity
import com.appetiser.itunesdiscover.features.auth.forgotpassword.newpassword.NewPasswordActivity
import com.appetiser.itunesdiscover.features.auth.forgotpassword.verification.ForgotPasswordVerificationActivity
import com.appetiser.itunesdiscover.features.auth.landing.LandingActivity
import com.appetiser.itunesdiscover.features.auth.login.LoginActivity
import com.appetiser.itunesdiscover.features.auth.register.RegisterActivity
import com.appetiser.itunesdiscover.features.auth.register.details.UserDetailsActivity
import com.appetiser.itunesdiscover.features.auth.register.verification.RegisterVerificationActivity
import com.appetiser.itunesdiscover.features.detail.DetailActivity
import com.appetiser.itunesdiscover.features.main.MainActivity
import com.appetiser.itunesdiscover.features.main.MediaRepositoryModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeEmailCheckActivity(): EmailCheckActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeRegisterVerificationActivity(): RegisterVerificationActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeRegisterActivity(): RegisterActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeForgotPasswordVerificationActivity(): ForgotPasswordVerificationActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeNewPasswordActivity(): NewPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeUserDetailsActivity(): UserDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(MediaRepositoryModule::class)])
    abstract fun contributeMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(MediaRepositoryModule::class)])
    abstract fun contributDetailActivity(): DetailActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [(AuthRepositoryModule::class)])
    abstract fun contributeLandingActivity(): LandingActivity
}
