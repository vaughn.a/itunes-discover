package com.appetiser.itunesdiscover.features.auth.forgotpassword

import android.os.Bundle
import com.appetiser.itunesdiscover.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class ForgotPasswordViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    private lateinit var email: String

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        email = bundle?.getString(ForgotPasswordActivity.KEY_EMAIL, "")!!
        disposables.add(Observable.just(email)
            .observeOn(schedulers.ui())
            .subscribeBy(onNext = { _state.onNext(ForgotPasswordState.GetEmail(it)) }))
    }

    private val _state by lazy {
        PublishSubject.create<ForgotPasswordState>()
    }

    val state: Observable<ForgotPasswordState> = _state

    fun forgotPassword(email: String) {
        repository
            .forgotPassword(email)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(ForgotPasswordState.ShowProgressLoading)
            }
            .doOnError {
                _state.onNext(ForgotPasswordState.HideProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(ForgotPasswordState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    if (it) {
                        _state.onNext(ForgotPasswordState.Success(true))
                    }
                }, onError = {
                _state.onNext(ForgotPasswordState.Error(it))
            }
            )
            .apply { disposables.add(this) }
    }
}
