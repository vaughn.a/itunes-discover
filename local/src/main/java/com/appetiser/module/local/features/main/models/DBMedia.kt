package com.appetiser.module.local.features.main.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.features.main.models.Media

@Entity(tableName = DBMedia.MEDIA_TABLE_NAME)
data class DBMedia(

    @ColumnInfo(name = "track_id")
    @PrimaryKey val trackId: Long = 0,

    @ColumnInfo(name = "track_name")
    val trackName: String = "",

    @ColumnInfo(name = "artist_name")
    val artistName: String = "",

    @ColumnInfo(name = "artwork_url100")
    val artworkUrl100: String = "",

    @ColumnInfo(name = "track_price")
    val trackPrice: Double = 0.0,

    val currency: String = "",
    val country: String = "",

    @ColumnInfo(name = "primary_genre_name")
    val primaryGenreName: String = "",

    @ColumnInfo(name = "short_description")
    val shortDescription: String = "",

    @ColumnInfo(name = "long_description")
    val longDescription: String = "",

    @ColumnInfo(name = "track_time_millis")
    val trackTimeMillis: Long = 0
) {
    companion object {
        const val MEDIA_TABLE_NAME = "media"
        const val EMPTY_USER_ID = "empty"

        fun fromDomain(media: Media): DBMedia {
            return with(media) {
                DBMedia(
                    this.trackId,
                    this.trackName,
                    this.artistName,
                    this.artworkUrl100,
                    this.trackPrice,
                    this.currency,
                    this.country,
                    this.primaryGenreName,
                    this.shortDescription,
                    this.longDescription,
                    this.trackTimeMillis
                )
            }
        }

        fun toDomain(dbMedia: DBMedia): Media {
            return with(dbMedia) {
                Media(
                    this.trackId,
                    this.trackName,
                    this.artistName,
                    this.artworkUrl100,
                    this.trackPrice,
                    this.currency,
                    this.country,
                    this.primaryGenreName,
                    this.shortDescription,
                    this.longDescription,
                    this.trackTimeMillis
                )
            }
        }
    }
}
