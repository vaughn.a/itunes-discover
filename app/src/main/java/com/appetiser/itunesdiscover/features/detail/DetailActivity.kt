package com.appetiser.itunesdiscover.features.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import com.appetiser.itunesdiscover.R
import com.appetiser.itunesdiscover.base.BaseViewModelActivity
import com.appetiser.itunesdiscover.databinding.ActivityDetailBinding

class DetailActivity : BaseViewModelActivity<ActivityDetailBinding, DetailViewModel>() {
    companion object {
        const val EXTRA_MEDIA_DATA = "extra_media_data"

        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    DetailActivity::class.java
                )
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setUpViewModelsObserver()
    }

    private fun setUpViewModelsObserver() {
        viewModel.media.observe(this,
            Observer {
                binding.item = (it)
            }
        )
    }
}
