package com.appetiser.module.data.features.auth

import com.appetiser.module.data.mapper.getAccessToken
import com.appetiser.module.data.mapper.getUserSession
import com.appetiser.module.domain.features.auth.models.AccessToken
import com.appetiser.module.domain.features.auth.models.CountryCode
import com.appetiser.module.domain.features.auth.models.UserSession
import com.appetiser.module.local.features.auth.AuthLocalSource
import com.appetiser.module.network.features.auth.AuthRemoteSource
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function3
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val remote: AuthRemoteSource,
    private val local: AuthLocalSource
) : AuthRepository {
    override fun checkEmail(email: String): Single<Boolean> = remote.checkEmail(email)

    override fun saveCredentials(user: UserSession): Single<UserSession> {
        return local.saveCredentials(user)
    }

    override fun saveToken(token: String): Single<AccessToken> {
        return local.saveToken(token)
    }

    override fun login(username: String, password: String): Single<Map<String, Any>> {
        return remote.login(username, password)
            .flatMap {
                saveUserCredentialsAndToken(it).singleOrError()
            }
    }

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    override fun socialLogin(
        accessToken: String,
        accessTokenProvider: String
    ): Single<Map<String, Any>> {
        return remote
            .socialLogin(accessToken, accessTokenProvider)
            .flatMap { saveUserCredentialsAndToken(it).singleOrError() }
    }

    override fun register(email: String, password: String, confirmPassword: String, mobileNumber: String, firstName: String, lastName: String): Single<Map<String, Any>> {
        return remote.register(email, password, confirmPassword, mobileNumber, firstName, lastName)
            .flatMap {
                saveUserCredentialsAndToken(it).singleOrError()
            }
    }

    override fun getUserSession(): Single<UserSession> {
        return local.getUserSession()
    }

    override fun verifyAccountEmail(accessToken: AccessToken, code: String): Single<Boolean> {
        return remote
            .verifyAccountEmail(accessToken, code)
            .flatMap { pair ->
                if (pair.first) {
                    // If verification succeeds. Save user to database.
                    saveCredentials(pair.second)
                        .map { pair.first }
                } else {
                    Single.just(pair.first)
                }
            }
    }

    override fun resendEmailVerificationCode(accessToken: AccessToken): Single<Boolean> {
        return remote.resendEmailVerificationCode(accessToken)
    }

    override fun updateUserSession(accessToken: AccessToken, userSession: UserSession): Single<UserSession> {
        return remote.updateUserSession(accessToken, userSession)
            .flatMap {
                saveCredentials(it)
            }
    }

    override fun forgotPassword(email: String): Single<Boolean> {
        return remote.forgotPassword(email)
    }

    override fun forgotPasswordCheckCode(email: String, code: String): Single<Boolean> {
        return remote.forgotPasswordCheckCode(email, code)
    }

    override fun newPassword(email: String, token: String, password: String, confirmPassword: String): Single<Boolean> {
        return remote.newPassword(email, token, password, confirmPassword)
    }

    override fun logout(): Completable {
        return local.logout()
    }

    override fun getAccessToken(): Single<AccessToken> {
        return local.getAccessToken()
    }

    override fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>> = remote.getCountryCode(countryCodes)

    private fun saveUserCredentialsAndToken(userSessionMapper: Map<String, Any>): Observable<Map<String, Any>> {
        val userSession = userSessionMapper.getUserSession()
        val accesstoken = userSessionMapper.getAccessToken()

        return Observable.zip(
            Observable.just(userSessionMapper), saveCredentials(userSession)
                .toObservable(), saveToken(accesstoken.token.orEmpty())
                .toObservable(),
            Function3 { mapper, _, _ ->
                return@Function3 mapper
            })
    }
}
