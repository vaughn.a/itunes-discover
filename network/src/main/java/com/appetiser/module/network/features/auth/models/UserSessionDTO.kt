package com.appetiser.module.network.features.auth.models

import com.appetiser.module.domain.features.auth.models.AccessToken
import com.appetiser.module.domain.features.auth.models.UserSession
import com.appetiser.module.network.features.auth.models.response.AuthDataResponse
import com.google.gson.annotations.SerializedName

class UserSessionDTO(
    @field:SerializedName("phone_number")
    val phoneNumber: String? = "",
    @field:SerializedName("email_verified")
    val emailVerified: Boolean = false,
    @field:SerializedName("phone_number_verified")
    val phoneNumberVerified: Boolean = false,
    val verified: Boolean = false,
    val email: String = ""
) : BaseUserDTO() {
    companion object {
        const val USER_KEY = "user"
        const val ACCESS_TOKEN_KEY = "accessToken"

        fun toDomain(userSessionDTO: UserSessionDTO): UserSession {
            return with(userSessionDTO) {
                UserSession(
                    phoneNumber = phoneNumber.orEmpty(),
                    emailVerified = emailVerified,
                    phoneNumberVerified = phoneNumberVerified,
                    verified = verified,
                    email = email,
                    lastName = lastName.orEmpty(),
                    firstName = firstName.orEmpty(),
                    fullName = fullName.orEmpty(),
                    avatarPermanentUrl = avatarPermanentUrl.orEmpty(),
                    avatarPermanentThumbUrl = avatarPermanentThumbUrl.orEmpty(),
                    id = id
                )
            }
        }

        fun mapAuthDataResponse(from: AuthDataResponse): Map<String, Any> {
            val map = mutableMapOf<String, Any>()
            val user = from.data.user
            val userData = from.data
            map[USER_KEY] = UserSession(
                phoneNumber = user.phoneNumber.orEmpty(),
                emailVerified = user.emailVerified,
                phoneNumberVerified = user.phoneNumberVerified,
                verified = user.verified,
                email = user.email,
                lastName = user.lastName.orEmpty(),
                firstName = user.firstName.orEmpty(),
                fullName = user.fullName.orEmpty(),
                avatarPermanentUrl = user.avatarPermanentUrl.orEmpty(),
                avatarPermanentThumbUrl = user.avatarPermanentThumbUrl.orEmpty(),
                id = user.id
            )

            map[ACCESS_TOKEN_KEY] = AccessToken(token = userData.accessToken, refresh = "", tokenType = userData.tokenType, expiresIn = userData.expiresIn)

            return map
        }
    }
}
