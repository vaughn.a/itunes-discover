package com.appetiser.itunesdiscover.features.main

import com.appetiser.itunesdiscover.features.main.models.MediaItem

sealed class MainState {

    // Progress
    object ShowProgressLoading : MainState()

    object HideProgressLoading : MainState()

    data class MediaTypeByIndex(val type: String) : MainState()

    data class MediaFeedData(val feed: List<MediaItem>) : MainState()
}
