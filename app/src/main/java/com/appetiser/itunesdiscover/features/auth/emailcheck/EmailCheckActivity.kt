package com.appetiser.itunesdiscover.features.auth.emailcheck

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.util.Patterns
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import com.appetiser.auth_modern.databinding.ActivityOnboardEmailBinding
import com.appetiser.itunesdiscover.R
import com.appetiser.itunesdiscover.base.BaseViewModelActivity
import com.appetiser.itunesdiscover.ext.enableWithAplhaWhen
import com.appetiser.itunesdiscover.features.auth.login.LoginActivity
import com.appetiser.itunesdiscover.features.auth.register.RegisterActivity
import com.appetiser.module.common.isEmailValid
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.spannableString
import com.appetiser.module.common.toast
import com.appetiser.module.network.base.response.error.ResponseError
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class EmailCheckActivity : BaseViewModelActivity<ActivityOnboardEmailBinding, EmailCheckViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, EmailCheckActivity::class.java)
            context.startActivity(intent)
        }

        private const val EMAIL = "email"
    }

    override fun canBack(): Boolean {
        return true
    }

    override fun getLayoutId(): Int = R.layout.activity_onboard_email

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setupViews(savedInstanceState)
        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {
                        is EmailCheckState.EmailExists -> {
                            LoginActivity.openActivity(this, state.email)
                        }

                        is EmailCheckState.EmailDoesNotExist -> {
                            RegisterActivity.openActivity(this, state.email)
                        }

                        is EmailCheckState.ShowProgressLoading -> {
                            toast("Sending request")
                        }

                        is EmailCheckState.HideProgressLoading -> {
                        }

                        is EmailCheckState.Error -> {
                            // Show error message
                            ResponseError.getError(
                                state.throwable,
                                ResponseError.ErrorCallback(httpExceptionCallback = {
                                    toast(it)
                                })
                            )
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun setupViews(savedInstanceState: Bundle?) {
        binding.etEmail.apply {
            setText(savedInstanceState?.getString(EMAIL))
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    val email = binding.etEmail.text.toString()

                    when {
                        email.isEmpty() -> toast(getString(R.string.email_must_not_be_empty))
                        !email.isEmailValid() -> toast(getString(R.string.invalid_email))
                        else -> viewModel.checkEmail(email)
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding
            .btnContinue
            .enableWithAplhaWhen(binding.etEmail) {
                Patterns.EMAIL_ADDRESS.matcher(it).matches()
            }

        binding
            .btnContinue
            .ninjaTap {
                viewModel
                    .checkEmail(
                        binding.etEmail.text.toString()
                    )
            }

        binding.privacy.movementMethod = LinkMovementMethod.getInstance()
        binding.privacy.text = getString(R.string.privacy_policy)
            .spannableString(
                this,
                null,
                R.font.inter_bold,
                ContextCompat.getColor(this, R.color.colorPrimary),
                "terms of service",
                "privacy policy",
                clickable = {
                    toast("Clicked $it")
                }
            )
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(EMAIL, binding.etEmail.text.toString())
    }
}
