package com.appetiser.module.domain.features.auth.models

data class Device(var width: Int, var height: Int)
