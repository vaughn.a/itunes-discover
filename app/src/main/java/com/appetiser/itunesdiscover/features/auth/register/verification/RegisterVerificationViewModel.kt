package com.appetiser.itunesdiscover.features.auth.register.verification

import android.os.Bundle
import com.appetiser.itunesdiscover.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class RegisterVerificationViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    private lateinit var email: String

    private val _state by lazy {
        PublishSubject.create<RegisterVerificationState>()
    }

    val state: Observable<RegisterVerificationState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        email = bundle?.getString(RegisterVerificationActivity.KEY_EMAIL, "").orEmpty()
        val indicatorVisible = bundle?.getBoolean(RegisterVerificationActivity.KEY_STEP_INDICATOR, false)!!

        disposables.add(
            Observable.just(Pair(email, indicatorVisible))
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .subscribeBy(
                    onNext = {
                        if (it.second)
                            _state.onNext(RegisterVerificationState.IndicatorVisible)
                        else
                            _state.onNext(RegisterVerificationState.IndicatorGone)

                        _state.onNext(RegisterVerificationState.GetEmail(it.first))
                    },
                    onError = {
                        Timber.e("Error $it")
                    }
                )
        )
    }

    fun resendToken() {
        disposables.add(repository.getAccessToken()
            .flatMap { repository.resendEmailVerificationCode(it) }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(RegisterVerificationState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(RegisterVerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(RegisterVerificationState.HideProgressLoading)
            }
            .subscribeBy(onSuccess = {
                _state.onNext(RegisterVerificationState.ResendCodeSuccess)
            }, onError = {
                _state.onNext(RegisterVerificationState.Error(it))
            }))
    }

    fun sendToken(token: String) {
        disposables.add(
            repository.getAccessToken()
                .flatMap { repository.verifyAccountEmail(it, token) }
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doOnSubscribe {
                    _state.onNext(RegisterVerificationState.ShowProgressLoading)
                }
                .doOnSuccess {
                    _state.onNext(RegisterVerificationState.HideProgressLoading)
                }
                .doOnError {
                    _state.onNext(RegisterVerificationState.HideProgressLoading)
                }
                .subscribeBy(onSuccess = {
                    _state.onNext(RegisterVerificationState.SendCodeSuccess(email, token))
                }, onError = {
                    _state.onNext(RegisterVerificationState.Error(it))
                }))
    }
}
