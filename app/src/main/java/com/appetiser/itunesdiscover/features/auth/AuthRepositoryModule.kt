package com.appetiser.itunesdiscover.features.auth

import com.appetiser.itunesdiscover.di.scopes.ActivityScope
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.auth.AuthRepositoryImpl
import com.appetiser.module.local.AppDatabase
import com.appetiser.module.local.features.auth.AuthLocalSource
import com.appetiser.module.local.features.auth.AuthLocalSourceImpl
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.auth.AuthRemoteSource
import com.appetiser.module.network.features.auth.AuthRemoteSourceImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides

@Module
class AuthRepositoryModule {

    @Provides
    fun providesAuthLocalSource(database: AppDatabase):
        AuthLocalSource = AuthLocalSourceImpl(database)

    @Provides
    fun providesAuthRemoteSource(
        baseplateApiServices: BaseplateApiServices,
        gson: Gson
    ): AuthRemoteSource = AuthRemoteSourceImpl(baseplateApiServices, gson)

    @ActivityScope
    @Provides
    fun providesAuthRepository(
        remote: AuthRemoteSource,
        local: AuthLocalSource
    ): AuthRepository = AuthRepositoryImpl(remote, local)
}
