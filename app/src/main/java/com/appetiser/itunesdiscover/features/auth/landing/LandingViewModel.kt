package com.appetiser.itunesdiscover.features.auth.landing

import android.os.Bundle
import com.appetiser.itunesdiscover.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.mapper.getUserSession
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class LandingViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val gson: Gson
) : BaseViewModel() {

    companion object {
        const val FACEBOOK = "facebook"
        const val GOOGLE = "google"
    }

    private val _state by lazy {
        PublishSubject.create<LandingState>()
    }

    val state: Observable<LandingState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        authRepository
            .getUserSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { userSession ->
                    if (userSession.id.isNullOrEmpty() || !userSession.verified) {
                        return@subscribeBy
                    }

                    if (userSession.hasOnboardingDetails()) {
                        _state
                            .onNext(
                                LandingState.UserIsLoggedIn
                            )
                    } else {
                        _state
                            .onNext(
                                LandingState.UserIsLoggedInButNotOnboarded
                            )
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    fun onFacebookLogin(facebookAccessToken: String) {
        socialLogin(facebookAccessToken, FACEBOOK)
    }

    fun onGoogleSignIn(googleAccessToken: String) {
        socialLogin(googleAccessToken, GOOGLE)
    }

    private fun socialLogin(
        accessToken: String,
        accessTokenProvider: String
    ) {
        authRepository
            .socialLogin(accessToken, accessTokenProvider)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(LandingState.ShowLoading(accessTokenProvider))
            }
            .doOnSuccess {
                _state.onNext(LandingState.HideLoading)
            }
            .doOnError {
                _state.onNext(LandingState.HideLoading)
            }
            .subscribeBy(
                onSuccess = { result ->
                    Timber.tag("socialLogin").d(gson.toJson(result))
                    val userSession = result.getUserSession()

                    if (userSession.hasOnboardingDetails()) {
                        _state.onNext(LandingState.SocialLoginSuccess)
                    } else {
                        _state.onNext(LandingState.SocialLoginSuccessButNotOnboarded)
                    }
                },
                onError = {
                    Timber.e(it)
                    _state.onNext(LandingState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }
}
