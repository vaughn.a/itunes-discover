package com.appetiser.itunesdiscover.features.auth.landing

sealed class LandingState {
    /**
     * User has logged in and has filled in onboarding details.
     */
    object UserIsLoggedIn : LandingState()

    /**
     * User has logged in but not filled in onboarding details.
     */
    object UserIsLoggedInButNotOnboarded : LandingState()

    class ShowLoading(val loginType: String) : LandingState()

    object HideLoading : LandingState()

    /**
     * User login success and filled with onboarding details.
     */
    object SocialLoginSuccess : LandingState()

    /**
     * User login success but not filled with onboarding details.
     * This can happend when backend cannot fetch user details from 3rd party provider.
     */
    object SocialLoginSuccessButNotOnboarded : LandingState()

    class Error(val throwable: Throwable) : LandingState()
}
