package com.appetiser.module.data.features.main

import android.util.Log
import androidx.lifecycle.LiveData
import com.appetiser.module.domain.features.main.models.Media
import com.appetiser.module.local.features.main.MediaLocalSource
import com.appetiser.module.network.features.main.MediaRemoteSource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MediaRepositoryImpl @Inject constructor(
    private val remote: MediaRemoteSource,
    private val local: MediaLocalSource
) : MediaRepository {

    override fun saveMediaFeed(feed: List<Media>): Single<List<Media>> {
        local.clear()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { Log.e("err", it.localizedMessage) },
                onComplete = {
                    feed.forEach {
                        local.saveMedia(it)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeBy(
                                onSuccess = { Log.e("saved", it.artistName) },
                                onError = { Log.e("err", it.localizedMessage) }
                            )
                    }
                }

            )
        return local.getAll()
    }

    override fun getMediaFromRemote(
        term: String,
        country: String,
        media: String
    ): Single<List<Media>> {
        val response = remote.getAll(term,
            country,
            media)

        return response
            .toObservable()
            .flatMapIterable {
                it.results
            }
            .toList()
    }

    override fun getMediaFeed(): Single<List<Media>> {
        return local.getAll()
    }

    override fun getLiveMediaFeed(): LiveData<List<Media>> {
        return local.getAllLiveData()
    }
}
