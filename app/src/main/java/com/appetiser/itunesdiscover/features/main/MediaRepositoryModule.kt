package com.appetiser.itunesdiscover.features.main

import com.appetiser.itunesdiscover.di.scopes.ActivityScope
import com.appetiser.module.data.features.main.MediaRepository
import com.appetiser.module.data.features.main.MediaRepositoryImpl
import com.appetiser.module.local.AppDatabase
import com.appetiser.module.local.features.main.MediaLocalSource
import com.appetiser.module.local.features.main.MediaLocalSourceImpl
import com.appetiser.module.network.ItunesApiService
import com.appetiser.module.network.features.main.MediaRemoteSource
import com.appetiser.module.network.features.main.MediaRemoteSourceImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides

@Module
class MediaRepositoryModule {

    @Provides
    fun providesAuthLocalSource(database: AppDatabase):
        MediaLocalSource = MediaLocalSourceImpl(database)

    @Provides
    fun providesAuthRemoteSource(
        itunesApiSservice: ItunesApiService,
        gson: Gson
    ): MediaRemoteSource = MediaRemoteSourceImpl(itunesApiSservice, gson)

    @ActivityScope
    @Provides
    fun providesMediaRepository(
        remote: MediaRemoteSource,
        local: MediaLocalSource
    ): MediaRepository = MediaRepositoryImpl(remote, local)
}
