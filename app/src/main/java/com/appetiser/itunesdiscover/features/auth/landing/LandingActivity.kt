package com.appetiser.itunesdiscover.features.auth.landing

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.auth_modern.databinding.ActivityLandingBinding
import com.appetiser.itunesdiscover.R
import com.appetiser.itunesdiscover.base.BaseViewModelActivity
import com.appetiser.itunesdiscover.features.auth.emailcheck.EmailCheckActivity
import com.appetiser.itunesdiscover.features.auth.register.details.UserDetailsActivity
import com.appetiser.itunesdiscover.features.main.MainActivity
import com.appetiser.module.auth_modern.FacebookLoginManager
import com.appetiser.module.auth_modern.GoogleSignInManager
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.setHidden
import com.appetiser.module.common.setVisible
import com.appetiser.module.common.toast
import com.google.gson.Gson
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class LandingActivity : BaseViewModelActivity<ActivityLandingBinding, LandingViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    LandingActivity::class.java
                )
            )
        }
    }

    private lateinit var facebookLoginManager: FacebookLoginManager
    private lateinit var googleSigninManager: GoogleSignInManager

    override fun getLayoutId(): Int = R.layout.activity_landing

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        facebookLoginManager = FacebookLoginManager()
        googleSigninManager = GoogleSignInManager(this)

        setupButtonListeners()
        setupViewModel()
    }

    override fun onDestroy() {
        facebookLoginManager.clearListeners()
        googleSigninManager.clearListeners()
        super.onDestroy()
    }

    private fun setupViewModel() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    handleState(state)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: LandingState) {
        when (state) {
            is LandingState.UserIsLoggedIn,
            LandingState.SocialLoginSuccess -> {
                openMainActivity()
            }
            is LandingState.UserIsLoggedInButNotOnboarded,
            is LandingState.SocialLoginSuccessButNotOnboarded -> {
                openUserDetailsActivity()
            }
            is LandingState.ShowLoading -> {
                showLoadingState(state.loginType)
            }
            is LandingState.HideLoading -> {
                hideLoadingState()
            }
            is LandingState.Error -> {
                toast(getString(R.string.generic_error))
            }
        }
    }

    private fun setupButtonListeners() {
        setupEmailButton()
        setupFacebookButton()
        setupGoogleButton()
    }

    private fun setupEmailButton() {
        binding
            .btnEmail
            .ninjaTap {
                EmailCheckActivity.openActivity(this)
            }
            .apply { disposables.add(this) }
    }

    private fun setupFacebookButton() {
        facebookLoginManager
            .setLoginSuccessListener { result ->
                Timber.d(Gson().toJson(result))
                viewModel.onFacebookLogin(result.accessToken.token)
            }

        facebookLoginManager
            .setLoginErrorListener {
                toast(getString(R.string.generic_error))
            }

        binding
            .btnFacebook
            .ninjaTap {
                Timber.d("clicked!")
                facebookLoginManager.login(this)
            }
            .apply { disposables.add(this) }
    }

    private fun setupGoogleButton() {
        googleSigninManager
            .setSignInSuccessListener { account ->
                Timber.d(Gson().toJson(account))
                viewModel.onGoogleSignIn(account.idToken!!)
            }

        googleSigninManager
            .setSignInErrorListener {
                toast(getString(R.string.generic_error))
            }

        binding
            .btnGoogle
            .ninjaTap {
                googleSigninManager.signIn()
            }
            .apply { disposables.add(this) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == GoogleSignInManager.REQUEST_CODE_GOOGLE_SIGN_IN) {
            googleSigninManager.sendSignInResult(data)
        } else {
            facebookLoginManager.sendLoginResult(requestCode, resultCode, data)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun showLoadingState(loginType: String) {
        when (loginType) {
            LandingViewModel.FACEBOOK -> {
                binding
                    .progressFacebook
                    .setVisible(true)

                binding
                    .imgFacebook
                    .setHidden(true)
            }
            LandingViewModel.GOOGLE -> {
                binding
                    .progressGoggle
                    .setVisible(true)

                binding
                    .imgGoogle
                    .setHidden(true)
            }
        }

        binding
            .btnEmail
            .apply {
                alpha = 0.5f
                isEnabled = false
            }

        binding
            .btnFacebook
            .apply {
                alpha = 0.5f
                isEnabled = false
            }
        binding
            .btnGoogle
            .apply {
                alpha = 0.5f
                isEnabled = false
            }
    }

    private fun hideLoadingState() {
        binding.progressFacebook.setVisible(false)
        binding.progressGoggle.setVisible(false)
        binding.imgFacebook.setHidden(false)
        binding.imgGoogle.setHidden(false)

        binding
            .btnEmail
            .apply {
                alpha = 1f
                isEnabled = true
            }

        binding
            .btnFacebook
            .apply {
                alpha = 1f
                isEnabled = true
            }
        binding
            .btnGoogle
            .apply {
                alpha = 1f
                isEnabled = true
            }
    }

    private fun openMainActivity() {
        MainActivity.openActivity(this)
        finishAffinity()
    }

    private fun openUserDetailsActivity() {
        UserDetailsActivity.openActivity(this)
        finishAffinity()
    }
}
