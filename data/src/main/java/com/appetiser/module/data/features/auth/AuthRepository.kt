package com.appetiser.module.data.features.auth

import com.appetiser.module.domain.features.auth.models.AccessToken
import com.appetiser.module.domain.features.auth.models.CountryCode
import com.appetiser.module.domain.features.auth.models.UserSession
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRepository {
    fun getAccessToken(): Single<AccessToken>

    fun checkEmail(email: String): Single<Boolean>

    fun login(username: String, password: String): Single<Map<String, Any>>

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    fun socialLogin(
        accessToken: String,
        accessTokenProvider: String
    ): Single<Map<String, Any>>

    fun saveCredentials(user: UserSession): Single<UserSession>

    fun updateUserSession(accessToken: AccessToken, userSession: UserSession): Single<UserSession>

    fun saveToken(token: String): Single<AccessToken>

    fun getUserSession(): Single<UserSession>

    fun register(
        email: String,
        password: String,
        confirmPassword: String,
        mobileNumber: String,
        firstName: String,
        lastName: String
    ): Single<Map<String, Any>>

    fun verifyAccountEmail(accessToken: AccessToken, code: String): Single<Boolean>

    fun resendEmailVerificationCode(accessToken: AccessToken): Single<Boolean>

    fun forgotPassword(email: String): Single<Boolean>

    fun forgotPasswordCheckCode(email: String, code: String): Single<Boolean>

    fun newPassword(email: String, token: String, password: String, confirmPassword: String): Single<Boolean>

    fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>>

    fun logout(): Completable
}
