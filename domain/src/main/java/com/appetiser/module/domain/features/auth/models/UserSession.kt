package com.appetiser.module.domain.features.auth.models

import com.google.gson.JsonObject

data class UserSession(
    val id: String? = "",
    val email: String = "",
    val lastName: String? = "",
    val firstName: String? = "",
    val fullName: String? = "",
    val avatarPermanentUrl: String? = "",
    val avatarPermanentThumbUrl: String? = "",
    val phoneNumber: String = "",
    val emailVerified: Boolean = false,
    val phoneNumberVerified: Boolean = false,
    val verified: Boolean = false
) {
    companion object {
        fun empty(): UserSession {
            return UserSession()
        }
    }

    fun toJsonStringExcludeEmpty(): String {
        val userJson = JsonObject()
            .apply {
                if (email.isNotEmpty()) addProperty("email", email)
                if (phoneNumber.isNotEmpty()) addProperty("phone_number", phoneNumber)
                if (!lastName.isNullOrEmpty()) addProperty("last_name", lastName)
                if (!firstName.isNullOrEmpty()) addProperty("first_name", firstName)
                if (!fullName.isNullOrEmpty()) addProperty("full_name", fullName)
            }

        return userJson
            .toString()
    }

    /**
     * Returns true if user onboarding details are present.
     */
    fun hasOnboardingDetails(): Boolean {
        return !lastName.isNullOrEmpty() || !firstName.isNullOrEmpty()
    }
}
