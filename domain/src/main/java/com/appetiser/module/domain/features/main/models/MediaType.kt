package com.appetiser.module.domain.features.main.models

import com.google.gson.annotations.SerializedName

enum class MediaType(val stringValue: String) {
    All("all"),
    Movie("movie"),
    Podcast("podcast"),
    Music("music"),
    Musicvideo("musicVideo"),
    Audiobook("audiobook"),
    Shortfilm("shortFilm"),
    Tvshow("tvShow"),
    Software("software"),
    Ebook("ebook")
}