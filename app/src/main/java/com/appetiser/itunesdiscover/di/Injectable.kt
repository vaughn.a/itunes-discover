package com.appetiser.itunesdiscover.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
