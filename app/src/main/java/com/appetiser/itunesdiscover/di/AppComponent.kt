package com.appetiser.itunesdiscover.di

import android.app.Application
import com.appetiser.itunesdiscover.ItunesdiscoverApplication
import com.appetiser.itunesdiscover.di.builders.ActivityBuilder
import com.appetiser.module.local.StorageModule
import com.appetiser.module.network.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton
import com.appetiser.module.data.mapper.MapperModule
import com.appetiser.module.local.DatabaseModule
import com.appetiser.module.network.ApiServiceModule

@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            MapperModule::class,
            StorageModule::class,
            DatabaseModule::class,
            NetworkModule::class,
            ApiServiceModule::class,
            ActivityBuilder::class,
            SchedulerModule::class
        ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: ItunesdiscoverApplication)
}
