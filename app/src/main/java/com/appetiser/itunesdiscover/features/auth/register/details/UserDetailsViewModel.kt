package com.appetiser.itunesdiscover.features.auth.register.details

import android.os.Bundle
import com.appetiser.itunesdiscover.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.features.auth.models.UserSession
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class UserDetailsViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
    }

    private val _state by lazy {
        PublishSubject.create<UserDetailsState>()
    }

    val state: Observable<UserDetailsState> = _state

    fun sendUserDetails(firstName: String, lastName: String) {
        disposables.add(
            repository.getAccessToken()
                .flatMap { accessToken ->
                    repository.updateUserSession(accessToken, UserSession(firstName = firstName, lastName = lastName))
                }
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doOnSubscribe {
                    _state.onNext(UserDetailsState.ShowProgressLoading)
                }
                .doOnSuccess {
                    _state.onNext(UserDetailsState.HideProgressLoading)
                }
                .doOnError {
                    _state.onNext(UserDetailsState.HideProgressLoading)
                }
                .subscribeBy(onSuccess = { user ->
                    if (!user.firstName.isNullOrEmpty() && !user.lastName.isNullOrEmpty()) {
                        _state.onNext(UserDetailsState.UserDetailsUpdated(user))
                    }
                }, onError = {
                    _state.onNext(UserDetailsState.Error(it))
                }))
    }
}
