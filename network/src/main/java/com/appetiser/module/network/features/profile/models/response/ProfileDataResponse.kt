package com.appetiser.module.network.features.profile.models.response

import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.auth.models.UserSessionDTO

data class ProfileDataResponse(val data: UserSessionDTO) : BaseResponse()
