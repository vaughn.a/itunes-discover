package com.appetiser.module.network.features.main.models

import com.appetiser.module.domain.features.main.models.Media

open class MediaDataResponse(
    val results: List<Media>
)
