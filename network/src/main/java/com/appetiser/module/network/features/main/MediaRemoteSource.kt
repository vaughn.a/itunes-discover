package com.appetiser.module.network.features.main

import com.appetiser.module.network.features.main.models.MediaDataResponse
import io.reactivex.Single

interface MediaRemoteSource {

    fun getAll(
        term: String,
        country: String,
        media: String
    ): Single<MediaDataResponse>
}
